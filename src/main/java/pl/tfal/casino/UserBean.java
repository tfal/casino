/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import pl.tfal.casino.encje.Uzytkownik;

@ManagedBean(name = "user")
@SessionScoped
public class UserBean implements Serializable
{
    private static final Logger logger = Logger.getLogger("pl.tfal.casino");
    @PersistenceContext(unitName = "casino_pu")
    private EntityManager em;
    private Long id;
    private String name;

    public Uzytkownik getUzytkownik()
    {
        TypedQuery<Uzytkownik> query = em.createNamedQuery("Uzytkowni.findByLogin", Uzytkownik.class);
        query.setParameter("login", getName());
        return query.getSingleResult();
    }

    public Long getId()
    {
        if (id == null)
        {
            id = getUzytkownik().getIdUzytkownicy();
        }
        return id;
    }

    public Long getIdImprezy()
    {
        return 1L;
    }

    public boolean isAdmin()
    {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        Object requestObject = context.getRequest();
        if (!(requestObject instanceof HttpServletRequest))
        {
            logger.severe("typ obiektu żądania: " + requestObject.getClass());
            return false;
        }
        HttpServletRequest request = (HttpServletRequest) requestObject;
        return request.isUserInRole("adminuser");
    }

    public String logout()
    {
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
        return "login";
    }

    private String getName()
    {
        if (name == null)
        {
            getUserData();
        }
        return name == null ? "" : name;
    }

    private void getUserData()
    {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        Object requestObject = context.getRequest();
        if (!(requestObject instanceof HttpServletRequest))
        {
            logger.severe("typ obiektu żądania: " + requestObject.getClass());
            return;
        }
        HttpServletRequest request = (HttpServletRequest) requestObject;
        name = request.getRemoteUser();
    }
}
