/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.db;

import java.util.Date;

/**
 *
 * @author tomek
 */
public class Wynik
{
    private Long idSpotkania;
    private String nazwaFazy;
    private Date dataSpotkania;
    private Long wynikDruzyny1;
    private Long wynikDruzyny2;
    private String nazwaDruzyny1;
    private String nazwaDruzyny2;
    private String flagaDruzyny1;
    private String flagaDruzyny2;

    public Long getIdSpotkania()
    {
        return idSpotkania;
    }

    public void setIdSpotkania(Long idSpotkania)
    {
        this.idSpotkania = idSpotkania;
    }

    public String getNazwaFazy()
    {
        return nazwaFazy;
    }

    public void setNazwaFazy(String nazwaFazy)
    {
        this.nazwaFazy = nazwaFazy;
    }

    public Date getDataSpotkania()
    {
        return dataSpotkania;
    }

    public void setDataSpotkania(Date dataSpotkania)
    {
        this.dataSpotkania = dataSpotkania;
    }

    public Long getWynikDruzyny1()
    {
        return wynikDruzyny1;
    }

    public void setWynikDruzyny1(Long wynikDruzyny1)
    {
        this.wynikDruzyny1 = wynikDruzyny1;
    }

    public Long getWynikDruzyny2()
    {
        return wynikDruzyny2;
    }

    public void setWynikDruzyny2(Long wynikDruzyny2)
    {
        this.wynikDruzyny2 = wynikDruzyny2;
    }

    public String getNazwaDruzyny1()
    {
        return nazwaDruzyny1;
    }

    public void setNazwaDruzyny1(String nazwaDruzyny1)
    {
        this.nazwaDruzyny1 = nazwaDruzyny1;
    }

    public String getNazwaDruzyny2()
    {
        return nazwaDruzyny2;
    }

    public void setNazwaDruzyny2(String nazwaDruzyny2)
    {
        this.nazwaDruzyny2 = nazwaDruzyny2;
    }

    public String getFlagaDruzyny1()
    {
        return flagaDruzyny1;
    }

    public void setFlagaDruzyny1(String flagaDruzyny1)
    {
        this.flagaDruzyny1 = flagaDruzyny1;
    }

    public String getFlagaDruzyny2()
    {
        return flagaDruzyny2;
    }

    public void setFlagaDruzyny2(String flagaDruzyny2)
    {
        this.flagaDruzyny2 = flagaDruzyny2;
    }
}
