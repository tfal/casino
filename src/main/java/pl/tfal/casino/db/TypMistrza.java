/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.db;

import java.util.Date;

/**
 *
 * @author tomek
 */
public class TypMistrza
{
    private Long idUzytkownika;
    private Long idDruzyny;
    private Date dataTypowania;
    private String nazwaDruzyny;
    private String flagaDruzyny;
    private String loginUzytkownika;
    private String nazwaUzytkownika;

    public Long getIdUzytkownika()
    {
        return idUzytkownika;
    }

    public void setIdUzytkownika(Long idUzytkownika)
    {
        this.idUzytkownika = idUzytkownika;
    }

    public Long getIdDruzyny()
    {
        return idDruzyny;
    }

    public void setIdDruzyny(Long idDruzyny)
    {
        this.idDruzyny = idDruzyny;
    }

    public Date getDataTypowania()
    {
        return dataTypowania;
    }

    public void setDataTypowania(Date dataTypowania)
    {
        this.dataTypowania = dataTypowania;
    }

    public String getNazwaDruzyny()
    {
        return nazwaDruzyny;
    }

    public void setNazwaDruzyny(String nazwaDruzyny)
    {
        this.nazwaDruzyny = nazwaDruzyny;
    }

    public String getFlagaDruzyny()
    {
        return flagaDruzyny;
    }

    public void setFlagaDruzyny(String flagaDruzyny)
    {
        this.flagaDruzyny = flagaDruzyny;
    }

    public String getLoginUzytkownika()
    {
        return loginUzytkownika;
    }

    public void setLoginUzytkownika(String loginUzytkownika)
    {
        this.loginUzytkownika = loginUzytkownika;
    }

    public String getNazwaUzytkownika()
    {
        return nazwaUzytkownika;
    }

    public void setNazwaUzytkownika(String nazwaUzytkownika)
    {
        this.nazwaUzytkownika = nazwaUzytkownika;
    }
}
