/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.db;

/**
 *
 * @author tomek
 */
public class Ranking
{
    private Long idUzytkownika;
    private String loginUzytkownika;
    private String nazwaUzytkownika;
    private int punktySuma;
    private int punktyStrataDoPoprzedniego;
    private int punktyStrataDoPierwszego;
    private int punktyMistrz;
    private int punktyIlosc1;
    private int punktyIlosc3;
    private int pozycja;
    private int zmianaPozycji;

    public Long getIdUzytkownika()
    {
        return idUzytkownika;
    }

    public void setIdUzytkownika(Long idUzytkownika)
    {
        this.idUzytkownika = idUzytkownika;
    }

    public String getLoginUzytkownika()
    {
        return loginUzytkownika;
    }

    public void setLoginUzytkownika(String loginUzytkownika)
    {
        this.loginUzytkownika = loginUzytkownika;
    }

    public String getNazwaUzytkownika()
    {
        return nazwaUzytkownika;
    }

    public void setNazwaUzytkownika(String nazwaUzytkownika)
    {
        this.nazwaUzytkownika = nazwaUzytkownika;
    }

    public int getPunktySuma()
    {
        return punktySuma;
    }

    public void setPunktySuma(int punktySuma)
    {
        this.punktySuma = punktySuma;
    }

    public int getPunktyStrataDoPoprzedniego()
    {
        return punktyStrataDoPoprzedniego;
    }

    public void setPunktyStrataDoPoprzedniego(int punktyStrataDoPoprzedniego)
    {
        this.punktyStrataDoPoprzedniego = punktyStrataDoPoprzedniego;
    }

    public int getPunktyStrataDoPierwszego()
    {
        return punktyStrataDoPierwszego;
    }

    public void setPunktyStrataDoPierwszego(int punktyStrataDoPierwszego)
    {
        this.punktyStrataDoPierwszego = punktyStrataDoPierwszego;
    }

    public int getPunktyMistrz()
    {
        return punktyMistrz;
    }

    public void setPunktyMistrz(int punktyMistrz)
    {
        this.punktyMistrz = punktyMistrz;
    }

    public int getPunktyIlosc1()
    {
        return punktyIlosc1;
    }

    public void setPunktyIlosc1(int punktyIlosc1)
    {
        this.punktyIlosc1 = punktyIlosc1;
    }

    public int getPunktyIlosc3()
    {
        return punktyIlosc3;
    }

    public void setPunktyIlosc3(int punktyIlosc3)
    {
        this.punktyIlosc3 = punktyIlosc3;
    }

    public int getPozycja()
    {
        return pozycja;
    }

    public void setPozycja(int pozycja)
    {
        this.pozycja = pozycja;
    }

    public int getZmianaPozycji()
    {
        return zmianaPozycji;
    }

    public void setZmianaPozycji(int zmianaPozycji)
    {
        this.zmianaPozycji = zmianaPozycji;
    }
}
