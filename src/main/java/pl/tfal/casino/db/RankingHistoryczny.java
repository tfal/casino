/*
Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package pl.tfal.casino.db;

/**
 *
 * @author tomek
 */
public class RankingHistoryczny
{
    private String osXOpis;
    private int punktySuma;
    private int pozycja;

    public String getOsXOpis()
    {
        return osXOpis;
    }

    public void setOsXOpis(String osXOpis)
    {
        this.osXOpis = osXOpis;
    }

    public int getPunktySuma()
    {
        return punktySuma;
    }

    public void setPunktySuma(int punktySuma)
    {
        this.punktySuma = punktySuma;
    }

    public int getPozycja()
    {
        return pozycja;
    }

    public void setPozycja(int pozycja)
    {
        this.pozycja = pozycja;
    }

}
