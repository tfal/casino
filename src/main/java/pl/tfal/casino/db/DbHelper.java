/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.DataSource;
import pl.tfal.casino.UserBean;

/**
 *
 * @author tomek
 */
@Stateless
@LocalBean
public class DbHelper
{
    private static final Logger logger = Logger.getLogger("pl.tfal.casino");
    @Resource(mappedName = "jdbc/casino")
    private DataSource ds;
    @Inject
    private UserBean user;

//    @PostConstruct
//    private void init()
//    {
//        try
//        {
//            InitialContext ctx = new InitialContext();
//            ds = (DataSource) ctx.lookup("jdbc/casino");
//        }
//        catch (NamingException ex)
//        {
//            logger.log(Level.SEVERE, null, ex);
//        }
//    }

    public List<Ranking> getRanking()
    {
        List<Ranking> wynik = new ArrayList<>();
        try
        {
            try (Connection conn = ds.getConnection())
            {
                String query = "SELECT * FROM TABLE(CASINO.RANKING_PELNY_LISTA(P_ID_IMPREZY => ?))";
                try (PreparedStatement stmt = conn.prepareStatement(query))
                {
                    stmt.setLong(1, user.getIdImprezy());
                    try (ResultSet rs = stmt.executeQuery())
                    {
                        while ( rs.next() )
                        {
                            Ranking r = new Ranking();
                            r.setIdUzytkownika(rs.getLong("ID_UZYTKOWNICY"));
                            r.setLoginUzytkownika(rs.getString("LOGIN_UZYTKOWNIKA"));
                            r.setNazwaUzytkownika(rs.getString("NAZWA_UZYTKOWNIKA"));
                            r.setPunktyIlosc1(rs.getInt("PUNKTY_ILOSC_1"));
                            r.setPunktyIlosc3(rs.getInt("PUNKTY_ILOSC_3"));
                            r.setPunktyMistrz(rs.getInt("PUNKTY_MISTRZ"));
                            r.setPunktySuma(rs.getInt("PUNKTY_SUMA"));
                            r.setPunktyStrataDoPierwszego(rs.getInt("PUNKTY_STRATA_DO_PIERW"));
                            r.setPunktyStrataDoPoprzedniego(rs.getInt("PUNKTY_STRATA_DO_POPRZ"));
                            r.setPozycja(rs.getInt("POZYCJA"));
                            r.setZmianaPozycji(rs.getInt("ZMIANA_POZYCJI"));
                            wynik.add(r);
                        }
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace(System.err);
        }
        return wynik;
    }

    public List<RankingHistoryczny> getRangingHistoryczny(Long idUzytkownika)
    {
        List<RankingHistoryczny> wynik = new ArrayList<>();
        try
        {
            try (Connection conn = ds.getConnection())
            {
                String query
                    = "SELECT * "
                    + "FROM TABLE(CASINO.RANKING_HIST_LISTA(P_ID_IMPREZY => ?, P_ID_UZYTKOWNICY => ?))";
                try (PreparedStatement stmt = conn.prepareStatement(query))
                {
                    stmt.setLong(1, user.getIdImprezy());
                    stmt.setLong(2, idUzytkownika);
                    try (ResultSet rs = stmt.executeQuery())
                    {
                        while ( rs.next() )
                        {
                            RankingHistoryczny r = new RankingHistoryczny();
                            r.setOsXOpis(rs.getString("OS_X_OPIS"));
                            r.setPozycja(rs.getInt("POZYCJA"));
                            r.setPunktySuma(rs.getInt("PUNKTY"));
                            wynik.add(r);
                        }
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace(System.err);
        }
        return wynik;
    }

    public List<SzczegolySpotkania> getSzczegolySpotkania(Long idSpotkania)
    {
        List<SzczegolySpotkania> wynik = new ArrayList<>();
        try
        {
            try (Connection conn = ds.getConnection())
            {
                String query = "SELECT * FROM TABLE(CASINO.WYNIKI_SPOTKANIA_LISTA(P_ID_SPOTKANIA => ?))";
                try (PreparedStatement stmt = conn.prepareStatement(query))
                {
                    stmt.setLong(1, idSpotkania);
                    try (ResultSet rs = stmt.executeQuery())
                    {
                        while ( rs.next() )
                        {
                            SzczegolySpotkania s = new SzczegolySpotkania();
                            s.setDataSpotkania(rs.getTimestamp("S_DATA"));
                            s.setDataTypowania(rs.getObject("T_DATA") == null ? null : rs.getTimestamp("T_DATA"));
                            s.setFlagaDruzyny1(rs.getString("D1_FLAGA"));
                            s.setFlagaDruzyny2(rs.getString("D2_FLAGA"));
                            s.setNazwaDruzyny1(rs.getString("D1_NAZWA"));
                            s.setNazwaDruzyny2(rs.getString("D2_NAZWA"));
                            s.setIdSpotkania(rs.getObject("ID_SPOTKANIA") == null ? null : rs.getLong("ID_SPOTKANIA"));
                            s.setIdTypu(rs.getObject("ID_TYPY") == null ? null : rs.getLong("ID_TYPY"));
                            s.setLoginUzytkownika(rs.getString("U_LOGIN"));
                            s.setPunkty(rs.getInt("PUNKTY"));
                            s.setTypDruzyny1(rs.getObject("T_WYNIK_1") == null ? null : rs.getLong("T_WYNIK_1"));
                            s.setTypDruzyny2(rs.getObject("T_WYNIK_2") == null ? null : rs.getLong("T_WYNIK_2"));
                            s.setNazwaUzytkownika(rs.getString("U_NAZWA"));
                            s.setWynikDruzyny1(rs.getObject("S_WYNIK_1") == null ? null : rs.getLong("S_WYNIK_1"));
                            s.setWynikDruzyny2(rs.getObject("S_WYNIK_2") == null ? null : rs.getLong("S_WYNIK_2"));
                            wynik.add(s);
                        }
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace(System.err);
        }
        return wynik;
    }

    public List<Typ> getWyniki()
    {
        List<Typ> wynik = new ArrayList<>();
        try
        {
            try (Connection conn = ds.getConnection())
            {
                String query
                    = "SELECT * "
                    + "FROM TABLE(CASINO.TYPY_LISTA(P_ID_IMPREZY => ?, P_ID_UZYTKOWNICY => ?)) WHERE S_DATA < SYSDATE "
                    + "ORDER BY S_DATA DESC";
                try (PreparedStatement stmt = conn.prepareStatement(query))
                {
                    stmt.setLong(1, user.getIdImprezy());
                    stmt.setLong(2, user.getId());
                    try (ResultSet rs = stmt.executeQuery())
                    {
                        while ( rs.next() )
                        {
                            Typ t = new Typ();
                            t.setDataSpotkania(rs.getTimestamp("S_DATA"));
                            t.setDataTypowania(rs.getObject("T_DATA") == null ? null : rs.getTimestamp("T_DATA"));
                            t.setFlagaDruzyny1(rs.getString("D1_FLAGA"));
                            t.setFlagaDruzyny2(rs.getString("D2_FLAGA"));
                            t.setNazwaDruzyny1(rs.getString("D1_NAZWA"));
                            t.setNazwaDruzyny2(rs.getString("D2_NAZWA"));
                            t.setNumerSpotkania(rs.getObject("S_NUMER") == null ? null : rs.getLong("S_NUMER"));
                            t.setIdSpotkania(rs.getObject("ID_SPOTKANIA") == null ? null : rs.getLong("ID_SPOTKANIA"));
                            t.setIdTypu(rs.getObject("ID_TYPY") == null ? null : rs.getLong("ID_TYPY"));
                            t.setLoginUzytkownika(rs.getString("U_LOGIN"));
                            t.setMoznaEdytowac(rs.getInt("MOZNA_TYPOWAC") == 1);
                            t.setNazwaFazy(rs.getString("F_NAZWA"));
                            t.setPunkty(rs.getInt("T_PUNKTY"));
                            t.setTypDruzyny1(rs.getObject("T_WYNIK_1") == null ? null : rs.getLong("T_WYNIK_1"));
                            t.setTypDruzyny2(rs.getObject("T_WYNIK_2") == null ? null : rs.getLong("T_WYNIK_2"));
                            t.setNazwaUzytkownika(rs.getString("U_NAZWA"));
                            t.setWynikDruzyny1(rs.getObject("S_WYNIK_1") == null ? null : rs.getLong("S_WYNIK_1"));
                            t.setWynikDruzyny2(rs.getObject("S_WYNIK_2") == null ? null : rs.getLong("S_WYNIK_2"));
                            wynik.add(t);
                        }
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace(System.err);
        }
        return wynik;
    }

    public List<Typ> getTypy()
    {
        List<Typ> wynik = new ArrayList<>();
        try
        {
            try (Connection conn = ds.getConnection())
            {
                String query
                    = "SELECT * "
                    + "FROM TABLE(CASINO.TYPY_LISTA(P_ID_IMPREZY => ?, P_ID_UZYTKOWNICY => ?)) WHERE S_DATA >= SYSDATE";
                try (PreparedStatement stmt = conn.prepareStatement(query))
                {
                    stmt.setLong(1, user.getIdImprezy());
                    stmt.setLong(2, user.getId());
                    try (ResultSet rs = stmt.executeQuery())
                    {
                        while ( rs.next() )
                        {
                            Typ t = new Typ();
                            t.setDataSpotkania(rs.getTimestamp("S_DATA"));
                            t.setDataTypowania(rs.getObject("T_DATA") == null ? null : rs.getTimestamp("T_DATA"));
                            t.setFlagaDruzyny1(rs.getString("D1_FLAGA"));
                            t.setFlagaDruzyny2(rs.getString("D2_FLAGA"));
                            t.setNazwaDruzyny1(rs.getString("D1_NAZWA"));
                            t.setNazwaDruzyny2(rs.getString("D2_NAZWA"));
                            t.setNumerSpotkania(rs.getObject("S_NUMER") == null ? null : rs.getLong("S_NUMER"));
                            t.setIdSpotkania(rs.getObject("ID_SPOTKANIA") == null ? null : rs.getLong("ID_SPOTKANIA"));
                            t.setIdTypu(rs.getObject("ID_TYPY") == null ? null : rs.getLong("ID_TYPY"));
                            t.setLoginUzytkownika(rs.getString("U_LOGIN"));
                            t.setMoznaEdytowac(rs.getInt("MOZNA_TYPOWAC") == 1);
                            t.setNazwaFazy(rs.getString("F_NAZWA"));
                            t.setPunkty(rs.getInt("T_PUNKTY"));
                            t.setTypDruzyny1(rs.getObject("T_WYNIK_1") == null ? null : rs.getLong("T_WYNIK_1"));
                            t.setTypDruzyny2(rs.getObject("T_WYNIK_2") == null ? null : rs.getLong("T_WYNIK_2"));
                            t.setNazwaUzytkownika(rs.getString("U_NAZWA"));
                            t.setWynikDruzyny1(rs.getObject("S_WYNIK_1") == null ? null : rs.getLong("S_WYNIK_1"));
                            t.setWynikDruzyny2(rs.getObject("S_WYNIK_2") == null ? null : rs.getLong("S_WYNIK_2"));
                            wynik.add(t);
                        }
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace(System.err);
        }
        return wynik;
    }

    public List<TypMistrza> getTypyMistrza()
    {
        List<TypMistrza> wynik = new ArrayList<>();
        try
        {
            try (Connection conn = ds.getConnection())
            {
                String query
                    = "SELECT * "
                    + "FROM TABLE(CASINO.TYPY_MISTRZ_LISTA(P_ID_IMPREZY => ?))";
                try (PreparedStatement stmt = conn.prepareStatement(query))
                {
                    stmt.setLong(1, user.getIdImprezy());
                    try (ResultSet rs = stmt.executeQuery())
                    {
                        while ( rs.next() )
                        {
                            TypMistrza t = new TypMistrza();
                            t.setDataTypowania(rs.getObject("DATA_TYPOWANIA") == null ? null : rs.getTimestamp("DATA_TYPOWANIA"));
                            t.setFlagaDruzyny(rs.getString("D_FLAGA"));
                            t.setIdDruzyny(rs.getObject("ID_DRUZYNY") == null ? null : rs.getLong("ID_DRUZYNY"));
                            t.setIdUzytkownika(rs.getObject("ID_UZYTKOWNICY") == null ? null : rs.getLong("ID_UZYTKOWNICY"));
                            t.setLoginUzytkownika(rs.getString("U_LOGIN"));
                            t.setNazwaDruzyny(rs.getString("D_NAZWA"));
                            t.setNazwaUzytkownika(rs.getString("U_NAZWA"));
                            wynik.add(t);
                        }
                    }
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace(System.err);
        }
        return wynik;
    }

    public void typujWynik(Typ typ) throws SQLException
    {
        try (Connection conn = ds.getConnection())
        {
            String query = "{CALL CASINO.TYPUJ_WYNIK(P_ID_UZYTKOWNICY => ?, P_ID_SPOTKANIA => ?, P_TYP_D1 => ?, P_TYP_D2 => ?)}";
            try (CallableStatement stmt = conn.prepareCall(query))
            {
                stmt.setLong(1, user.getId());
                stmt.setLong(2, typ.getIdSpotkania());
                stmt.setObject(3, typ.getTypDruzyny1());
                stmt.setObject(4, typ.getTypDruzyny2());
                stmt.executeQuery();
            }
            logger.info(String.format("typowanie [użytkownik: %s (%d), spotkanie: %d, typ: %s:%s]",
                user.getUzytkownik().getNazwa(),
                user.getId(),
                typ.getIdSpotkania(),
                typ.getTypDruzyny1(),
                typ.getTypDruzyny2()));
        }
    }

    public Long getIdMistrza() throws SQLException
    {
        try (Connection conn = ds.getConnection())
        {
            String q = "SELECT ID_DRUZYNY_MISTRZ FROM IMPREZY_UZYTKOWNICY WHERE ID_UZYTKOWNICY = ? AND ID_IMPREZY = ?";
            try (PreparedStatement stmt = conn.prepareStatement(q))
            {
                stmt.setLong(1, user.getId());
                stmt.setLong(2, user.getIdImprezy());
                try (ResultSet rs = stmt.executeQuery())
                {
                    if ( rs.next() )
                    {
                        return rs.getObject("ID_DRUZYNY_MISTRZ") != null ? rs.getLong("ID_DRUZYNY_MISTRZ") : null;
                    }
                }
            }
        }
        return null;
    }

    public void typujMistrza(Long idDruzynyMistrz) throws SQLException
    {
        try (Connection conn = ds.getConnection())
        {
            String query = "{CALL CASINO.TYPUJ_MISTRZA(P_ID_IMPREZY => ?, P_ID_UZYTKOWNICY => ?, P_ID_DRUZYNY_MISTRZ => ?)}";
            try (CallableStatement stmt = conn.prepareCall(query))
            {
                stmt.setLong(1, user.getIdImprezy());
                stmt.setLong(2, user.getId());
                stmt.setLong(3, idDruzynyMistrz);
                stmt.executeQuery();
            }
            logger.info(String.format("typowanie mistrza [użytkownik: %s (%d), mistrz: %s", user.getUzytkownik().getNazwa(), user.getId(), idDruzynyMistrz));
        }
    }

    public void przeliczPunkty() throws SQLException
    {
        try (Connection conn = ds.getConnection())
        {
            String query = "{CALL CASINO.PRZELICZ_PUNKTY}";
            try (CallableStatement stmt = conn.prepareCall(query))
            {
                stmt.executeQuery();
            }
            logger.info("przeliczenie punktów");
        }
    }

    public void uaktualnijWynik(Typ typ) throws SQLException
    {
        try (Connection conn = ds.getConnection())
        {
            String query = "{CALL CASINO.UAKTUALNIJ_WYNIK(P_ID_SPOTKANIA => ?, P_WYNIK_D1 => ?, P_WYNIK_D2 => ?)}";
            try (CallableStatement stmt = conn.prepareCall(query))
            {
                stmt.setLong(1, typ.getIdSpotkania());
                stmt.setObject(2, typ.getWynikDruzyny1());
                stmt.setObject(3, typ.getWynikDruzyny2());
                stmt.executeQuery();
            }
            logger.info(String.format("uaktualnienie wyniku [spotkanie: %d, wyniki: %s:%s]",
                typ.getIdSpotkania(),
                typ.getWynikDruzyny1(),
                typ.getWynikDruzyny2()));
        }
    }
}
