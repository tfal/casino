/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.views;

import pl.tfal.casino.db.Ranking;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import pl.tfal.casino.db.DbHelper;

/**
 *
 * @author tomek
 */
@ManagedBean(name = "rankingView")
@RequestScoped
public class RankingView implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Inject
    private DbHelper dbHelper;

    public List<Ranking> getRanking()
    {
        return dbHelper.getRanking();
    }
}
