/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.views;

import pl.tfal.casino.db.Typ;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;
import pl.tfal.casino.UserBean;
import pl.tfal.casino.db.DbHelper;

/**
 *
 * @author tomek
 */
@ManagedBean(name = "typyView")
@ViewScoped
public class TypyView implements Serializable
{
    private static final Logger logger = Logger.getLogger("pl.tfal.casino");
    private static final long serialVersionUID = 1L;
    @Inject
    private DbHelper dbHelper;
    @Inject
    private UserBean user;
    private List<Typ> typy;

    @PostConstruct
    public void init()
    {
        if (user.isAdmin())
        {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            try
            {
                context.redirect(context.getRequestContextPath() + "/wyniki.xhtml");
            }
            catch (IOException ex)
            {
                logger.severe(ex.getMessage());
            }
        }
        else
        {
            typy = dbHelper.getTypy();
        }
    }

    public List<Typ> getTypy()
    {
        return typy;
    }

    public void onRowUpdate(RowEditEvent event)
    {
        try
        {
            Object o = event.getObject();
            if (o != null)
            {
                Typ t = (Typ) o;
                if (t.isMoznaEdytowac())
                {
                    dbHelper.typujWynik(t);
                    init();

                    FacesMessage msg = new FacesMessage("Zmieniono przewidywany wynik spotkania",
                            String.format("%s vs %s", t.getNazwaDruzyny1() != null ? t.getNazwaDruzyny1() : "<brak>", t.getNazwaDruzyny2() != null ? t.getNazwaDruzyny2() : "<brak>"));
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.err);
            FacesMessage msg = new FacesMessage("Wystąpił błąd", e.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowCancel(RowEditEvent event)
    {
        init();
    }
}
