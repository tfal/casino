/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.views;

import pl.tfal.casino.db.RankingHistoryczny;
import pl.tfal.casino.db.DbHelper;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.tfal.casino.encje.Uzytkownik;

@ManagedBean(name = "rankingHistorycznyView")
@RequestScoped
public class RankingHistorycznyView implements Serializable
{
    private static final long serialVersionUID = 1L;
    @PersistenceContext(unitName = "casino_pu")
    private EntityManager em;
    @Inject
    private DbHelper dbHelper;
    private List<RankingHistoryczny> rankingHistoryczny;

    public List<RankingHistoryczny> getRankingHistoryczny()
    {
        return dbHelper.getRangingHistoryczny(getIdUzytkownika());
    }

    public Uzytkownik getUzytkownik()
    {
        return em.find(Uzytkownik.class, getIdUzytkownika());
    }

    private Long getIdUzytkownika()
    {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, String> parameterMap = (Map<String, String>) context.getRequestParameterMap();
        String idUzytkownika = parameterMap.get("idUzytkownika");
        return idUzytkownika != null ? Long.parseLong(idUzytkownika) : 0L;
    }
}
