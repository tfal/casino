/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.views;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import pl.tfal.casino.UserBean;
import pl.tfal.casino.encje.Log;

@ManagedBean(name = "logiView")
@ViewScoped
public class LogiView implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger("pl.tfal.casino");
    @PersistenceContext(unitName = "casino_pu")
    private EntityManager em;
    @Inject
    private UserBean user;
    private LazyDataModel<Log> lazyModel;

    @PostConstruct
    public void init()
    {
        if (!user.isAdmin())
        {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            try
            {
                context.redirect(context.getRequestContextPath() + "/wyniki.xhtml");
            }
            catch (IOException ex)
            {
                logger.severe(ex.getMessage());
            }
        }

        lazyModel = new LazyDataModel<Log>()
        {
            private static final long serialVersionUID = 1L;
            private List<Log> logs;

            @Override
            public List<Log> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters)
            {
                TypedQuery<Long> cntQuery = em.createNamedQuery("Log.countAll", Long.class);
                setRowCount(cntQuery.getSingleResult().intValue());

                TypedQuery<Log> query = em.createNamedQuery("Log.findAll", Log.class);
                logs = query.setFirstResult(first).setMaxResults(pageSize).getResultList();
                return logs;
            }

            @Override
            public Log getRowData(String rowKey)
            {
                if (rowKey != null)
                {
                    for (final Log log : logs)
                    {
                        if (rowKey.equals(Long.toString(log.getIdLogi())))
                        {
                            return log;
                        }
                    }
                }
                return null;
            }

            @Override
            public Object getRowKey(Log log)
            {
                return log.getIdLogi();
            }
        };
    }

    public LazyDataModel<Log> getLazyModel()
    {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Log> lazyModel)
    {
        this.lazyModel = lazyModel;
    }
}
