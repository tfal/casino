/*
Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package pl.tfal.casino.views;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import pl.tfal.casino.UserBean;
import pl.tfal.casino.encje.Uzytkownik;

/**
 *
 * @author tomek
 */
@ManagedBean(name = "zmianaHaslaView")
@ViewScoped
public class ZmianaHaslaView implements Serializable
{
    private static final Logger logger = Logger.getLogger("pl.tfal.casino");
    private static final long serialVersionUID = 1L;
    @PersistenceContext(unitName = "casino_pu")
    private EntityManager em;
    @Resource
    private UserTransaction utx;
    @Inject
    private UserBean user;
    private String stareHaslo;
    private String noweHaslo;
    private String noweHaslo2;

    public String getStareHaslo()
    {
        return stareHaslo;
    }

    public void setStareHaslo(String stareHaslo)
    {
        this.stareHaslo = stareHaslo;
    }

    public String getNoweHaslo()
    {
        return noweHaslo;
    }

    public void setNoweHaslo(String noweHaslo)
    {
        this.noweHaslo = noweHaslo;
    }

    public String getNoweHaslo2()
    {
        return noweHaslo2;
    }

    public void setNoweHaslo2(String noweHaslo2)
    {
        this.noweHaslo2 = noweHaslo2;
    }

    private String md5(String input)
    {
        try
        {
            MessageDigest mDigest = MessageDigest.getInstance("MD5");
            byte[] result = mDigest.digest(input.getBytes());
            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i < result.length; i++ )
            {
                sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString().toLowerCase();
        }
        catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(ZmianaHaslaView.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void zmienHaslo(ActionEvent actionEvent)
    {
        if ( getNoweHaslo() == null || getNoweHaslo().isEmpty() || !getNoweHaslo().equals(getNoweHaslo2()) )
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Zmiana hasła nie powiodła się", "Puste hasło lub hasła nieprawidłowe!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }

        if ( getNoweHaslo().length() < 4 )
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Zmiana hasła nie powiodła się", "Hasło musi składać się z co najmniej 4 znaki!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }

        Uzytkownik u = em.find(Uzytkownik.class, user.getId());
        if ( u.getHaslo().equals(md5(getStareHaslo())) )
        {
            try
            {
                utx.begin();
                em.joinTransaction();
                u.setHaslo(md5(getNoweHaslo()));
                em.merge(u);
                utx.commit();

                logger.info(String.format("zmiana hasła [%s]", u.getNazwa()));

                FacesMessage msg = new FacesMessage("Hasło zmieniono poprawnie", null);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            catch (Exception e)
            {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Wystąpił błąd podczas zmiany hasła!", "Nieznany błąd!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
        else
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Zmiana hasła nie powiodła się", "Bieżące hasło jest niepoprawne!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
