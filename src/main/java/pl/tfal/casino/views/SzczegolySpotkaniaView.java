/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.tfal.casino.db.DbHelper;
import pl.tfal.casino.db.SzczegolySpotkania;
import pl.tfal.casino.encje.Spotkanie;

/**
 *
 * @author tomek
 */
@ManagedBean(name = "szczegolySpotkaniaView")
@ViewScoped
public class SzczegolySpotkaniaView implements Serializable
{
    private static final long serialVersionUID = 1L;
    @PersistenceContext(unitName = "casino_pu")
    private EntityManager em;
    @Inject
    private DbHelper dbHelper;
    private List<SzczegolySpotkania> szczegoly;

    @PostConstruct
    public void init()
    {
        szczegoly = new ArrayList<>();

        if (getSpotkanie().getData().after(new Date()))
        {
            return;
        }

        szczegoly = dbHelper.getSzczegolySpotkania(getIdSpotkania());
    }

    public List<SzczegolySpotkania> getSzczegoly()
    {
        return szczegoly;
    }

    public Spotkanie getSpotkanie()
    {
        return em.find(Spotkanie.class, getIdSpotkania());
    }

    private Long getIdSpotkania()
    {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, String> parameterMap = (Map<String, String>) context.getRequestParameterMap();
        String idSpotkania = parameterMap.get("idSpotkania");
        return idSpotkania != null ? Long.parseLong(idSpotkania) : 0L;
    }
}
