/*
 Copyright 2014 Tomasz Fal tomasz.fal@gmail.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package pl.tfal.casino.views;

import pl.tfal.casino.db.Typ;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;
import pl.tfal.casino.db.DbHelper;

/**
 *
 * @author tomek
 */
@ManagedBean(name = "wynikiView")
@ViewScoped
public class WynikiView implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Inject
    private DbHelper dbHelper;
    private List<Typ> wyniki;

    @PostConstruct
    public void init()
    {
        wyniki = dbHelper.getWyniki();
    }

    public List<Typ> getWyniki()
    {
        return wyniki;
    }

    public void przeliczPunkty(ActionEvent actionEvent)
    {
        try
        {
            dbHelper.przeliczPunkty();
            FacesMessage msg = new FacesMessage("Przeliczono punkty", null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.err);
            FacesMessage msg = new FacesMessage("Wystąpił błąd", e.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowUpdate(RowEditEvent event)
    {
        try
        {
            Object o = event.getObject();
            if (o != null)
            {
                Typ t = (Typ) o;
                dbHelper.uaktualnijWynik(t);
                init();
                FacesMessage msg = new FacesMessage("Zmieniono wynik spotkania", t.getNazwaDruzyny1() + " vs " + t.getNazwaDruzyny2());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.err);
            FacesMessage msg = new FacesMessage("Wystąpił błąd", e.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowCancel(RowEditEvent event)
    {
        init();
    }
}
