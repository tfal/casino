/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tomek
 */
@Embeddable
public class ImprezaUzytkownikaPK implements Serializable
{

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_IMPREZY")
    private BigInteger idImprezy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_UZYTKOWNICY")
    private BigInteger idUzytkownicy;

    public ImprezaUzytkownikaPK()
    {
    }

    public ImprezaUzytkownikaPK(BigInteger idImprezy, BigInteger idUzytkownicy)
    {
        this.idImprezy = idImprezy;
        this.idUzytkownicy = idUzytkownicy;
    }

    public BigInteger getIdImprezy()
    {
        return idImprezy;
    }

    public void setIdImprezy(BigInteger idImprezy)
    {
        this.idImprezy = idImprezy;
    }

    public BigInteger getIdUzytkownicy()
    {
        return idUzytkownicy;
    }

    public void setIdUzytkownicy(BigInteger idUzytkownicy)
    {
        this.idUzytkownicy = idUzytkownicy;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idImprezy != null ? idImprezy.hashCode() : 0);
        hash += (idUzytkownicy != null ? idUzytkownicy.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImprezaUzytkownikaPK))
        {
            return false;
        }
        ImprezaUzytkownikaPK other = (ImprezaUzytkownikaPK) object;
        if ((this.idImprezy == null && other.idImprezy != null) || (this.idImprezy != null && !this.idImprezy.equals(other.idImprezy)))
        {
            return false;
        }
        if ((this.idUzytkownicy == null && other.idUzytkownicy != null) || (this.idUzytkownicy != null && !this.idUzytkownicy.equals(other.idUzytkownicy)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.ImprezaUzytkownikaPK[ idImprezy=" + idImprezy + ", idUzytkownicy=" + idUzytkownicy + " ]";
    }

}
