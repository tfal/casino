/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "IMPREZY")
@SequenceGenerator(allocationSize = 1, name = "G_IMPREZY", sequenceName = "G_IMPREZY")
@NamedQueries(
    {
        @NamedQuery(name = "Impreza.findAll", query = "SELECT i FROM Impreza i")
    })
public class Impreza implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(generator = "G_IMPREZY", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_IMPREZY")
    private Long idImprezy;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAZWA")
    private String nazwa;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_KONCA")
    private Date dataKonca;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MAKS_DATA_TYPOWANIA_MISTRZA")
    private Date maksDataTypowaniaMistrza;
    @JoinColumn(name = "ID_DRUZYNY_MISTRZ", referencedColumnName = "ID_DRUZYNY")
    @ManyToOne(optional = true)
    private Druzyna mistrz;
    @OneToMany(mappedBy = "impreza")
    private List<Faza> fazy;

    public Impreza()
    {
    }

    public Impreza(Long idImprezy)
    {
        this.idImprezy = idImprezy;
    }

    public Long getIdImprezy()
    {
        return idImprezy;
    }

    public void setIdImprezy(Long idImprezy)
    {
        this.idImprezy = idImprezy;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public Date getDataKonca()
    {
        return dataKonca;
    }

    public void setDataKonca(Date dataKonca)
    {
        this.dataKonca = dataKonca;
    }
    
    public Date getMaksDataTypowaniaMistrza()
    {
        return maksDataTypowaniaMistrza;
    }

    public void setMaksDataTypowaniaMistrza(Date maksDataTypowaniaMistrza)
    {
        this.maksDataTypowaniaMistrza = maksDataTypowaniaMistrza;
    }

    public Druzyna getMistrz()
    {
        return mistrz;
    }

    public void setMistrz(Druzyna mistrz)
    {
        this.mistrz = mistrz;
    }

    public List<Faza> getFazy()
    {
        return fazy;
    }

    public void setFazy(List<Faza> fazy)
    {
        this.fazy = fazy;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idImprezy != null ? idImprezy.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if ( !(object instanceof Impreza) )
        {
            return false;
        }
        Impreza other = (Impreza) object;
        if ( (this.idImprezy == null && other.idImprezy != null) || (this.idImprezy != null && !this.idImprezy.equals(other.idImprezy)) )
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.Impreza[ idImprezy=" + idImprezy + " ]";
    }
}
