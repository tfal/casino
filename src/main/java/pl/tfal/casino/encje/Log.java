/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "LOGI")
@NamedQueries(
        {
            @NamedQuery(name = "Log.findAll", query = "SELECT l FROM Log l ORDER BY l.idLogi DESC"),
            @NamedQuery(name = "Log.countAll", query = "SELECT count(l) from Log l")
        })
public class Log implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Column(name = "ID_LOGI")
    private Long idLogi;
    @NotNull
    @Column(name = "DATA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @JoinColumn(name = "ID_UZYTKOWNICY", referencedColumnName = "ID_UZYTKOWNICY")
    @ManyToOne(optional = true)
    private Uzytkownik uzytkownik;
    @Size(max = 100)
    @Column(name = "FUNKCJA")
    private String funkcja;
    @Size(max = 4000)
    @Column(name = "TRESC")
    private String tresc;

    public Log()
    {
    }

    public Log(Long idLogi)
    {
        this.idLogi = idLogi;
    }

    public Long getIdLogi()
    {
        return idLogi;
    }

    public void setIdLogi(Long idLogi)
    {
        this.idLogi = idLogi;
    }

    public Date getData()
    {
        return data;
    }

    public void setData(Date data)
    {
        this.data = data;
    }

    public Uzytkownik getUzytkownik()
    {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik)
    {
        this.uzytkownik = uzytkownik;
    }

    public String getFunkcja()
    {
        return funkcja;
    }

    public void setFunkcja(String funkcja)
    {
        this.funkcja = funkcja;
    }

    public String getTresc()
    {
        return tresc;
    }

    public void setTresc(String tresc)
    {
        this.tresc = tresc;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idLogi != null ? idLogi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Log))
        {
            return false;
        }
        Log other = (Log) object;
        if ((this.idLogi == null && other.idLogi != null) || (this.idLogi != null && !this.idLogi.equals(other.idLogi)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.encje.Log[ idLogi=" + idLogi + " ]";
    }

}
