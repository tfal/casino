/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "TYPY")
@SequenceGenerator(allocationSize = 1, name = "G_TYPY", sequenceName = "G_TYPY")
@NamedQueries(
        {
            @NamedQuery(name = "Typ.findAll", query = "SELECT t FROM Typ t"),
            @NamedQuery(name = "Typ.findByIdUzytkownika", query = "SELECT t FROM Typ t WHERE t.uzytkownik.idUzytkownicy = :idUzytkownicy")
        })
public class Typ implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(generator = "G_TYPY", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_TYPY")
    private Long idTypy;
    @NotNull
    @Column(name = "DATA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @JoinColumn(name = "ID_SPOTKANIA", referencedColumnName = "ID_SPOTKANIA")
    @ManyToOne(optional = false)
    private Spotkanie spotkanie;
    @JoinColumn(name = "ID_UZYTKOWNICY", referencedColumnName = "ID_UZYTKOWNICY")
    @ManyToOne(optional = false)
    private Uzytkownik uzytkownik;
    @NotNull
    @Column(name = "WYNIK_1")
    private Long wynik1;
    @NotNull
    @Column(name = "WYNIK_2")
    private Long wynik2;
    @Column(name = "PUNKTY")
    private Long punkty;

    public Typ()
    {
    }

    public Typ(Long idTypy)
    {
        this.idTypy = idTypy;
    }

    public Long getIdTypy()
    {
        return idTypy;
    }

    public void setIdTypy(Long idTypy)
    {
        this.idTypy = idTypy;
    }

    public Date getData()
    {
        return data;
    }

    public void setData(Date data)
    {
        this.data = data;
    }

    public Long getWynik1()
    {
        return wynik1;
    }

    public void setWynik1(Long wynik1)
    {
        this.wynik1 = wynik1;
    }

    public Long getWynik2()
    {
        return wynik2;
    }

    public void setWynik2(Long wynik2)
    {
        this.wynik2 = wynik2;
    }

    public Long getPunkty()
    {
        return punkty;
    }

    public void setPunkty(Long punkty)
    {
        this.punkty = punkty;
    }

    public Spotkanie getSpotkanie()
    {
        return spotkanie;
    }

    public void setSpotkanie(Spotkanie spotkanie)
    {
        this.spotkanie = spotkanie;
    }

    public Uzytkownik getUzytkownik()
    {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik)
    {
        this.uzytkownik = uzytkownik;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idTypy != null ? idTypy.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typ))
        {
            return false;
        }
        Typ other = (Typ) object;
        if ((this.idTypy == null && other.idTypy != null) || (this.idTypy != null && !this.idTypy.equals(other.idTypy)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.Typ[ idTypy=" + idTypy + " ]";
    }
}
