/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "UZYTKOWNICY")
@SequenceGenerator(allocationSize = 1, name = "G_UZYTKOWNICY", sequenceName = "G_UZYTKOWNICY")
@NamedQueries(
        {
            @NamedQuery(name = "Uzytkownik.findAll", query = "SELECT u FROM Uzytkownik u"),
            @NamedQuery(name = "Uzytkowni.findByLogin", query = "SELECT u FROM Uzytkownik u WHERE u.login = :login")
        })
public class Uzytkownik implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(generator = "G_UZYTKOWNICY", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_UZYTKOWNICY")
    private Long idUzytkownicy;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LOGIN")
    private String login;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "HASLO")
    private String haslo;
    @Size(max = 50)
    @Column(name = "NAZWA")
    private String nazwa;
    @NotNull
    @Column(name = "AKTYWNY")
    private Character aktywny;
    @OneToMany(mappedBy = "uzytkownik")
    private List<ImprezaUzytkownika> imprezy;
    @OneToMany(mappedBy = "uzytkownik")
    private List<Typ> typy;

    public Uzytkownik()
    {
    }

    public Uzytkownik(Long idUzytkownicy)
    {
        this.idUzytkownicy = idUzytkownicy;
    }

    public Long getIdUzytkownicy()
    {
        return idUzytkownicy;
    }

    public void setIdUzytkownicy(Long idUzytkownicy)
    {
        this.idUzytkownicy = idUzytkownicy;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getHaslo()
    {
        return haslo;
    }

    public void setHaslo(String haslo)
    {
        this.haslo = haslo;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public Character getAktywny()
    {
        return aktywny;
    }

    public void setAktywny(Character aktywny)
    {
        this.aktywny = aktywny;
    }

    public List<ImprezaUzytkownika> getImprezy()
    {
        return imprezy;
    }

    public void setImprezy(List<ImprezaUzytkownika> imprezy)
    {
        this.imprezy = imprezy;
    }

    public List<Typ> getTypy()
    {
        return typy;
    }

    public void setTypy(List<Typ> typy)
    {
        this.typy = typy;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idUzytkownicy != null ? idUzytkownicy.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uzytkownik))
        {
            return false;
        }
        Uzytkownik other = (Uzytkownik) object;
        if ((this.idUzytkownicy == null && other.idUzytkownicy != null) || (this.idUzytkownicy != null && !this.idUzytkownicy.equals(other.idUzytkownicy)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.Uzytkownik[ idUzytkownicy=" + idUzytkownicy + " ]";
    }
}
