/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Embeddable
public class GrupaUzytkownikowPK implements Serializable
{
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LOGIN")
    private String login;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "GRUPA")
    private String grupa;

    public GrupaUzytkownikowPK()
    {
    }

    public GrupaUzytkownikowPK(String login, String grupa)
    {
        this.login = login;
        this.grupa = grupa;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getGrupa()
    {
        return grupa;
    }

    public void setGrupa(String grupa)
    {
        this.grupa = grupa;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (login != null ? login.hashCode() : 0);
        hash += (grupa != null ? grupa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupaUzytkownikowPK))
        {
            return false;
        }
        GrupaUzytkownikowPK other = (GrupaUzytkownikowPK) object;
        if ((this.login == null && other.login != null) || (this.login != null && !this.login.equals(other.login)))
        {
            return false;
        }
        if ((this.grupa == null && other.grupa != null) || (this.grupa != null && !this.grupa.equals(other.grupa)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.GrupaUzytkownikowPK[ login=" + login + ", grupa=" + grupa + " ]";
    }
}
