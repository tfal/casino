/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "FAZY")
@SequenceGenerator(allocationSize = 1, name = "G_FAZY", sequenceName = "G_FAZY")
@NamedQueries(
        {
            @NamedQuery(name = "Faza.findAll", query = "SELECT f FROM Faza f")
        })
public class Faza implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(generator = "G_FAZY", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_FAZY")
    private Long idFazy;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAZWA")
    private String nazwa;
    @JoinColumn(name = "ID_IMPREZY", referencedColumnName = "ID_IMPREZY")
    @ManyToOne(optional = false)
    private Impreza impreza;
    @OneToMany(mappedBy = "faza")
    private List<Spotkanie> spotkania;

    public Faza()
    {
    }

    public Faza(Long idFazy)
    {
        this.idFazy = idFazy;
    }

    public Long getIdFazy()
    {
        return idFazy;
    }

    public void setIdFazy(Long idFazy)
    {
        this.idFazy = idFazy;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public Impreza getIdImprezy()
    {
        return impreza;
    }

    public void setImpreza(Impreza impreza)
    {
        this.impreza = impreza;
    }

    public List<Spotkanie> getSpotkania()
    {
        return spotkania;
    }

    public void setSpotkania(List<Spotkanie> spotkania)
    {
        this.spotkania = spotkania;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idFazy != null ? idFazy.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Faza))
        {
            return false;
        }
        Faza other = (Faza) object;
        if ((this.idFazy == null && other.idFazy != null) || (this.idFazy != null && !this.idFazy.equals(other.idFazy)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.Faza[ idFazy=" + idFazy + " ]";
    }
}
