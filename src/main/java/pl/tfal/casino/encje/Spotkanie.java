/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "SPOTKANIA")
@SequenceGenerator(allocationSize = 1, name = "G_SPOTKANIA", sequenceName = "G_SPOTKANIA")
@NamedQueries(
        {
            @NamedQuery(name = "Spotkanie.findAll", query = "SELECT s FROM Spotkanie s ORDER BY s.data DESC")
        })
public class Spotkanie implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(generator = "G_SPOTKANIA", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_SPOTKANIA")
    private Long idSpotkania;
    @Column(name = "NUMER")
    private Long numer;
    @Column(name = "DATA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Size(max = 100)
    @Column(name = "OPIS")
    private String opis;
    @JoinColumn(name = "ID_FAZY", referencedColumnName = "ID_FAZY")
    @ManyToOne(optional = false)
    private Faza faza;
    @JoinColumn(name = "ID_DRUZYNY_1", referencedColumnName = "ID_DRUZYNY")
    @ManyToOne(optional = true)
    private Druzyna druzyna1;
    @JoinColumn(name = "ID_DRUZYNY_2", referencedColumnName = "ID_DRUZYNY")
    @ManyToOne(optional = true)
    private Druzyna druzyna2;
    @Column(name = "WYNIK_1")
    private Long wynik1;
    @Column(name = "WYNIK_2")
    private Long wynik2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "spotkanie")
    private List<Typ> typy;

    public Spotkanie()
    {
    }

    public Spotkanie(Long idSpotkania)
    {
        this.idSpotkania = idSpotkania;
    }

    public Long getIdSpotkania()
    {
        return idSpotkania;
    }

    public void setIdSpotkania(Long idSpotkania)
    {
        this.idSpotkania = idSpotkania;
    }

    public Long getNumer()
    {
        return numer;
    }

    public void setNumer(Long numer)
    {
        this.numer = numer;
    }

    public Date getData()
    {
        return data;
    }

    public void setData(Date data)
    {
        this.data = data;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public Long getWynik1()
    {
        return wynik1;
    }

    public void setWynik1(Long wynik1)
    {
        this.wynik1 = wynik1;
    }

    public Long getWynik2()
    {
        return wynik2;
    }

    public void setWynik2(Long wynik2)
    {
        this.wynik2 = wynik2;
    }

    public List<Typ> getTypy()
    {
        return typy;
    }

    public void setTypy(List<Typ> typy)
    {
        this.typy = typy;
    }

    public Druzyna getDruzyna1()
    {
        return druzyna1;
    }

    public void setDruzyna1(Druzyna druzyna1)
    {
        this.druzyna1 = druzyna1;
    }

    public Druzyna getDruzyna2()
    {
        return druzyna2;
    }

    public void setDruzyna2(Druzyna druzyna2)
    {
        this.druzyna2 = druzyna2;
    }

    public Faza getFaza()
    {
        return faza;
    }

    public void setFaza(Faza faza)
    {
        this.faza = faza;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idSpotkania != null ? idSpotkania.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Spotkanie))
        {
            return false;
        }
        Spotkanie other = (Spotkanie) object;
        if ((this.idSpotkania == null && other.idSpotkania != null) || (this.idSpotkania != null && !this.idSpotkania.equals(other.idSpotkania)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.Spotkanie[ idSpotkania=" + idSpotkania + " ]";
    }
}
