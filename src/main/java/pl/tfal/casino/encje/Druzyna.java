/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "DRUZYNY")
@SequenceGenerator(allocationSize = 1, name = "G_DRUZYNY", sequenceName = "G_DRUZYNY")
@NamedQueries(
    {
        @NamedQuery(name = "Druzyna.findAll", query = "SELECT d FROM Druzyna d ORDER BY d.nazwa")
    })
public class Druzyna implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(generator = "G_DRUZYNY", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_DRUZYNY")
    private Long idDruzyny;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAZWA")
    private String nazwa;
    @Size(max = 50)
    @Column(name = "FLAGA")
    private String flaga;

    public Druzyna()
    {
    }

    public Druzyna(Long idDruzyny)
    {
        this.idDruzyny = idDruzyny;
    }

    public Long getIdDruzyny()
    {
        return idDruzyny;
    }

    public void setIdDruzyny(Long idDruzyny)
    {
        this.idDruzyny = idDruzyny;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public String getFlaga()
    {
        return flaga;
    }

    public void setFlaga(String flaga)
    {
        this.flaga = flaga;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idDruzyny != null ? idDruzyny.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if ( !(object instanceof Druzyna) )
        {
            return false;
        }
        Druzyna other = (Druzyna) object;
        if ( (this.idDruzyny == null && other.idDruzyny != null) || (this.idDruzyny != null && !this.idDruzyny.equals(other.idDruzyny)) )
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.Druzyna[ idDruzyny=" + idDruzyny + " ]";
    }
}
