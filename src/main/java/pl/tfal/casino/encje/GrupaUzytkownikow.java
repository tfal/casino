/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "GRUPY_UZYTKOWNIKOW")
@NamedQueries(
        {
            @NamedQuery(name = "GrupaUzytkownikow.findAll", query = "SELECT g FROM GrupaUzytkownikow g")
        })
public class GrupaUzytkownikow implements Serializable
{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GrupaUzytkownikowPK grupaUzytkownikowPK;

    public GrupaUzytkownikow()
    {
    }

    public GrupaUzytkownikow(GrupaUzytkownikowPK grupaUzytkownikowPK)
    {
        this.grupaUzytkownikowPK = grupaUzytkownikowPK;
    }

    public GrupaUzytkownikow(String login, String grupa)
    {
        this.grupaUzytkownikowPK = new GrupaUzytkownikowPK(login, grupa);
    }

    public GrupaUzytkownikowPK getGrupaUzytkownikowPK()
    {
        return grupaUzytkownikowPK;
    }

    public void setGrupaUzytkownikowPK(GrupaUzytkownikowPK grupaUzytkownikowPK)
    {
        this.grupaUzytkownikowPK = grupaUzytkownikowPK;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (grupaUzytkownikowPK != null ? grupaUzytkownikowPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupaUzytkownikow))
        {
            return false;
        }
        GrupaUzytkownikow other = (GrupaUzytkownikow) object;
        if ((this.grupaUzytkownikowPK == null && other.grupaUzytkownikowPK != null) || (this.grupaUzytkownikowPK != null && !this.grupaUzytkownikowPK.equals(other.grupaUzytkownikowPK)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.GrupaUzytkownikow[ grupaUzytkownikowPK=" + grupaUzytkownikowPK + " ]";
    }
}
