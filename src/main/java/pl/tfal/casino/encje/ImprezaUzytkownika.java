/*
 * Autor: Tomasz Fal
 */
package pl.tfal.casino.encje;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author tomek
 */
@Entity
@Table(name = "IMPREZY_UZYTKOWNICY")
@NamedQueries(
        {
            @NamedQuery(name = "ImprezaUzytkownika.findAll", query = "SELECT i FROM ImprezaUzytkownika i")
        })
public class ImprezaUzytkownika implements Serializable
{

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ImprezaUzytkownikaPK imprezaUzytkownikaPK;
    @JoinColumn(name = "ID_DRUZYNY_MISTRZ", referencedColumnName = "ID_DRUZYNY")
    @ManyToOne
    private Druzyna mistrz;
    @JoinColumn(name = "ID_IMPREZY", referencedColumnName = "ID_IMPREZY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Impreza impreza;
    @JoinColumn(name = "ID_UZYTKOWNICY", referencedColumnName = "ID_UZYTKOWNICY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Uzytkownik uzytkownik;

    public ImprezaUzytkownika()
    {
    }

    public ImprezaUzytkownika(ImprezaUzytkownikaPK imprezaUzytkownikaPK)
    {
        this.imprezaUzytkownikaPK = imprezaUzytkownikaPK;
    }

    public ImprezaUzytkownika(BigInteger idImprezy, BigInteger idUzytkownicy)
    {
        this.imprezaUzytkownikaPK = new ImprezaUzytkownikaPK(idImprezy, idUzytkownicy);
    }

    public ImprezaUzytkownikaPK getImprezaUzytkownikaPK()
    {
        return imprezaUzytkownikaPK;
    }

    public void setImprezaUzytkownikaPK(ImprezaUzytkownikaPK imprezaUzytkownikaPK)
    {
        this.imprezaUzytkownikaPK = imprezaUzytkownikaPK;
    }

    public Druzyna getMistrz()
    {
        return mistrz;
    }

    public void setMistrz(Druzyna mistrz)
    {
        this.mistrz = mistrz;
    }

    public Impreza getImpreza()
    {
        return impreza;
    }

    public void setImpreza(Impreza impreza)
    {
        this.impreza = impreza;
    }

    public Uzytkownik getUzytkownik()
    {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik)
    {
        this.uzytkownik = uzytkownik;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (imprezaUzytkownikaPK != null ? imprezaUzytkownikaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImprezaUzytkownika))
        {
            return false;
        }
        ImprezaUzytkownika other = (ImprezaUzytkownika) object;
        if ((this.imprezaUzytkownikaPK == null && other.imprezaUzytkownikaPK != null) || (this.imprezaUzytkownikaPK != null && !this.imprezaUzytkownikaPK.equals(other.imprezaUzytkownikaPK)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "pl.tfal.casino.entities.ImprezaUzytkownika[ imprezaUzytkownikaPK=" + imprezaUzytkownikaPK + " ]";
    }

}
