CREATE OR REPLACE PACKAGE CASINO IS

TYPE T_WYNIK_R IS RECORD
(
  ID_SPOTKANIA  SPOTKANIA.ID_SPOTKANIA%TYPE,
  F_NAZWA       FAZY.NAZWA%TYPE,
  S_NUMER       SPOTKANIA.NUMER%TYPE,
  S_DATA        SPOTKANIA.DATA%TYPE,
  S_WYNIK_1     SPOTKANIA.WYNIK_1%TYPE,
  S_WYNIK_2     SPOTKANIA.WYNIK_2%TYPE,
  D1_NAZWA      DRUZYNY.NAZWA%TYPE,
  D2_NAZWA      DRUZYNY.NAZWA%TYPE,
  D1_FLAGA      DRUZYNY.FLAGA%TYPE,
  D2_FLAGA      DRUZYNY.FLAGA%TYPE
);
TYPE T_WYNIK_T IS TABLE OF T_WYNIK_R;
FUNCTION WYNIKI_LISTA(P_ID_IMPREZY IN NUMBER) RETURN T_WYNIK_T PIPELINED;

FUNCTION DAJ_WYNIK_SPOTKANIA(P_ID_SPOTKANIA IN NUMBER) RETURN T_WYNIK_T PIPELINED;

TYPE T_TYP_R IS RECORD
(
  ID_SPOTKANIA  SPOTKANIA.ID_SPOTKANIA%TYPE,
  ID_TYPY       TYPY.ID_TYPY%TYPE,
  F_NAZWA       FAZY.NAZWA%TYPE,
  S_NUMER       SPOTKANIA.NUMER%TYPE,
  S_DATA        SPOTKANIA.DATA%TYPE,
  S_WYNIK_1     SPOTKANIA.WYNIK_1%TYPE,
  S_WYNIK_2     SPOTKANIA.WYNIK_2%TYPE,
  D1_NAZWA      DRUZYNY.NAZWA%TYPE,
  D2_NAZWA      DRUZYNY.NAZWA%TYPE,
  D1_FLAGA      DRUZYNY.FLAGA%TYPE,
  D2_FLAGA      DRUZYNY.FLAGA%TYPE,
  U_LOGIN       UZYTKOWNICY.LOGIN%TYPE,
  U_NAZWA       UZYTKOWNICY.NAZWA%TYPE,
  T_DATA        TYPY.DATA%TYPE,
  T_WYNIK_1     TYPY.WYNIK_1%TYPE,
  T_WYNIK_2     TYPY.WYNIK_2%TYPE,
  T_PUNKTY      TYPY.PUNKTY%TYPE,
  MOZNA_TYPOWAC NUMBER
);
TYPE T_TYP_T IS TABLE OF T_TYP_R;
FUNCTION TYPY_LISTA(P_ID_IMPREZY IN NUMBER, P_ID_UZYTKOWNICY IN NUMBER) RETURN T_TYP_T PIPELINED;

TYPE T_WYNIK_SPOTKANIA_R IS RECORD
(
  ID_SPOTKANIA  SPOTKANIA.ID_SPOTKANIA%TYPE,
  ID_TYPY       TYPY.ID_TYPY%TYPE,
  S_NUMER       SPOTKANIA.NUMER%TYPE,
  S_DATA        SPOTKANIA.DATA%TYPE,
  D1_NAZWA      DRUZYNY.NAZWA%TYPE,
  D2_NAZWA      DRUZYNY.NAZWA%TYPE,
  D1_FLAGA      DRUZYNY.FLAGA%TYPE,
  D2_FLAGA      DRUZYNY.FLAGA%TYPE,
  S_WYNIK_1     SPOTKANIA.WYNIK_1%TYPE,
  S_WYNIK_2     SPOTKANIA.WYNIK_2%TYPE,
  T_DATA        TYPY.DATA%TYPE,
  T_WYNIK_1     TYPY.WYNIK_1%TYPE,
  T_WYNIK_2     TYPY.WYNIK_2%TYPE,
  PUNKTY        TYPY.PUNKTY%TYPE,
  U_LOGIN       UZYTKOWNICY.LOGIN%TYPE,
  U_NAZWA       UZYTKOWNICY.NAZWA%TYPE
);
TYPE T_WYNIK_SPOTKANIA_T IS TABLE OF T_WYNIK_SPOTKANIA_R;
FUNCTION WYNIKI_SPOTKANIA_LISTA(P_ID_SPOTKANIA IN NUMBER) RETURN T_WYNIK_SPOTKANIA_T PIPELINED;

TYPE T_RANKING_R IS RECORD
(
  ID_UZYTKOWNICY          UZYTKOWNICY.ID_UZYTKOWNICY%TYPE,
  LOGIN_UZYTKOWNIKA       UZYTKOWNICY.LOGIN%TYPE,
  NAZWA_UZYTKOWNIKA       UZYTKOWNICY.NAZWA%TYPE,
  PUNKTY_SUMA             NUMBER,
  PUNKTY_STRATA_DO_POPRZ  NUMBER,
  PUNKTY_STRATA_DO_PIERW  NUMBER,
  PUNKTY_MISTRZ           NUMBER,
  PUNKTY_ILOSC_3          NUMBER,
  PUNKTY_ILOSC_1          NUMBER,
  POZYCJA                 NUMBER

);
TYPE T_RANKING_T IS TABLE OF T_RANKING_R;
FUNCTION RANKING_LISTA(P_ID_IMPREZY IN NUMBER, P_NA_DZIEN IN DATE DEFAULT SYSDATE) RETURN T_RANKING_T PIPELINED;

TYPE T_RANKING_PELNY_R IS RECORD
(
  ID_UZYTKOWNICY          UZYTKOWNICY.ID_UZYTKOWNICY%TYPE,
  LOGIN_UZYTKOWNIKA       UZYTKOWNICY.LOGIN%TYPE,
  NAZWA_UZYTKOWNIKA       UZYTKOWNICY.NAZWA%TYPE,
  PUNKTY_SUMA             NUMBER,
  PUNKTY_STRATA_DO_POPRZ  NUMBER,
  PUNKTY_STRATA_DO_PIERW  NUMBER,
  PUNKTY_MISTRZ           NUMBER,
  PUNKTY_ILOSC_3          NUMBER,
  PUNKTY_ILOSC_1          NUMBER,
  POZYCJA                 NUMBER,
  POPRZEDNIA_POZYCJA      NUMBER,
  ZMIANA_POZYCJI          NUMBER,
  POPRZEDNIE_PUNKTY_SUMA  NUMBER,
  ZMIANA_PUNKTOW          NUMBER
);
TYPE T_RANKING_PELNY_T IS TABLE OF T_RANKING_PELNY_R;
FUNCTION RANKING_PELNY_LISTA(P_ID_IMPREZY IN NUMBER, P_NA_DZIEN IN DATE DEFAULT SYSDATE) RETURN T_RANKING_PELNY_T PIPELINED;

TYPE T_UZYTKOWNIK_T IS TABLE OF UZYTKOWNICY%ROWTYPE;
FUNCTION UZYTKOWNICY_LISTA(P_ID_IMPREZY IN NUMBER) RETURN T_UZYTKOWNIK_T PIPELINED;

FUNCTION DAJ_DANE_UZYTKOWNIKA(P_ID_UZYTKOWNICY IN NUMBER) RETURN T_UZYTKOWNIK_T PIPELINED;

TYPE T_RANKING_HIST_R IS RECORD
(
  OS_X_OPIS         VARCHAR2(10),
  POZYCJA           NUMBER,
  PUNKTY            NUMBER
);
TYPE T_RANKING_HIST_T IS TABLE OF T_RANKING_HIST_R;
FUNCTION RANKING_HIST_LISTA(P_ID_IMPREZY IN NUMBER, P_ID_UZYTKOWNICY IN NUMBER) RETURN T_RANKING_HIST_T PIPELINED;

TYPE T_TYP_MISTRZ_R IS RECORD
(
  ID_UZYTKOWNICY    IMPREZY_UZYTKOWNICY.ID_UZYTKOWNICY%TYPE,
  ID_DRUZYNY        IMPREZY_UZYTKOWNICY.ID_DRUZYNY_MISTRZ%TYPE,
  DATA_TYPOWANIA    IMPREZY_UZYTKOWNICY.DATA_TYPOWANIA_MISTRZ%TYPE,
  U_LOGIN           UZYTKOWNICY.LOGIN%TYPE,
  U_NAZWA           UZYTKOWNICY.NAZWA%TYPE,
  D_NAZWA           DRUZYNY.NAZWA%TYPE,
  D_FLAGA           DRUZYNY.FLAGA%TYPE
);
TYPE T_TYP_MISTRZ_T IS TABLE OF T_TYP_MISTRZ_R;
FUNCTION TYPY_MISTRZ_LISTA(P_ID_IMPREZY IN NUMBER) RETURN T_TYP_MISTRZ_T PIPELINED;

PROCEDURE TYPUJ_WYNIK(P_ID_UZYTKOWNICY IN NUMBER, P_ID_SPOTKANIA IN NUMBER,  P_TYP_D1 IN NUMBER, P_TYP_D2 IN NUMBER, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F');

PROCEDURE TYPUJ_WYNIK2(P_ID_IMPREZY IN NUMBER, P_LOGIN IN VARCHAR2, P_NUMER_SPOTKANIA IN NUMBER, P_TYP_D1 IN NUMBER, P_TYP_D2 IN NUMBER, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F');

PROCEDURE TYPUJ_MISTRZA(P_ID_IMPREZY IN NUMBER, P_LOGIN IN VARCHAR2, P_DRUZYNA_MISTRZ IN VARCHAR2, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F');

PROCEDURE TYPUJ_MISTRZA2(P_ID_IMPREZY IN NUMBER, P_ID_UZYTKOWNICY IN NUMBER, P_ID_DRUZYNY_MISTRZ IN NUMBER, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F');

PROCEDURE UAKTUALNIJ_WYNIK(P_ID_SPOTKANIA IN NUMBER, P_WYNIK_D1 IN NUMBER, P_WYNIK_D2 IN NUMBER);

PROCEDURE PRZELICZ_PUNKTY;

PROCEDURE DODAJ_SPOTKANIE(P_ID_IMPREZY IN NUMBER, P_FAZA IN VARCHAR2, P_NUMER IN NUMBER, P_DATA_SPOTKANIA IN VARCHAR, P_DRUZYNA_1 IN VARCHAR2, P_DRUZYNA_2 IN VARCHAR2, P_OPIS IN VARCHAR2 DEFAULT NULL);

PROCEDURE UAKTUALNIJ_SPOTKANIE(P_ID_IMPREZY IN NUMBER, P_NUMER IN NUMBER, P_DRUZYNA_1 IN VARCHAR2, P_DRUZYNA_2 IN VARCHAR2);

PROCEDURE DODAJ_UZYTKOWNIKA(P_LOGIN IN VARCHAR2, P_HASLO IN VARCHAR2, P_HASH IN CHAR, P_ID_IMPREZY IN NUMBER DEFAULT NULL, P_NAZWA IN VARCHAR2 DEFAULT NULL, P_ADMIN IN CHAR DEFAULT 'F');

PROCEDURE USTAW_HASLO_UZYTKOWNIKA(P_LOGIN IN VARCHAR2, P_HASLO IN VARCHAR2);

END CASINO;
/

CREATE OR REPLACE PACKAGE BODY CASINO IS

C_PUNKTY_1 CONSTANT NUMBER := 1;
C_PUNKTY_3 CONSTANT NUMBER := 3;
C_PUNKTY_MISTRZ CONSTANT NUMBER := 5;
C_MAKS_GLOSOWANIE CONSTANT NUMBER := 900/86400;
C_ROLA_NORMALUSER CONSTANT VARCHAR2(20) := 'normaluser';
C_ROLA_ADMINUSER CONSTANT VARCHAR2(20) := 'adminuser';

PROCEDURE LOGUJ(P_ID_UZYTKOWNICY IN NUMBER, P_FUNKCJA IN VARCHAR2, P_TRESC IN VARCHAR2) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  INSERT INTO LOGI
    (ID_UZYTKOWNICY, FUNKCJA, TRESC) VALUES
    (P_ID_UZYTKOWNICY, P_FUNKCJA, P_TRESC);
  COMMIT;
END LOGUJ;

FUNCTION OBLICZ_PUNKTY(WYN_D1 IN NUMBER, WYN_D2 IN NUMBER, TYP_D1 IN NUMBER, TYP_D2 IN NUMBER) RETURN NUMBER IS
BEGIN
  IF TYP_D1 IS NULL OR TYP_D2 IS NULL OR WYN_D1 IS NULL OR WYN_D2 IS NULL THEN
    RETURN NULL;
  END IF;

  IF WYN_D1 = TYP_D1 AND WYN_D2 = TYP_D2 THEN
    RETURN C_PUNKTY_3;
  END IF;

  IF WYN_D1 = WYN_D2 AND TYP_D1 = TYP_D2 THEN
    RETURN C_PUNKTY_1;
  END IF;

  IF WYN_D1 - WYN_D2 > 0 AND TYP_D1 - TYP_D2 > 0 THEN
    RETURN C_PUNKTY_1;
  END IF;

  IF WYN_D1 - WYN_D2 < 0 AND TYP_D1 - TYP_D2 < 0 THEN
    RETURN C_PUNKTY_1;
  END IF;

  RETURN 0;
END OBLICZ_PUNKTY;

FUNCTION WYNIKI_LISTA(P_ID_IMPREZY IN NUMBER) RETURN T_WYNIK_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT
      S.ID_SPOTKANIA,
      F.NAZWA F_NAZWA,
      S.NUMER S_NUMER,
      S.DATA S_DATA,
      S.WYNIK_1 S_WYNIK_1,
      S.WYNIK_2 S_WYNIK_2,
      D1.NAZWA D1_NAZWA,
      D2.NAZWA D2_NAZWA,
      D1.FLAGA D1_FLAGA,
      D2.FLAGA D2_FLAGA
    FROM
      SPOTKANIA S
      JOIN FAZY F ON S.ID_FAZY = F.ID_FAZY
      JOIN IMPREZY I ON F.ID_IMPREZY = I.ID_IMPREZY
      LEFT JOIN DRUZYNY D1 ON S.ID_DRUZYNY_1 = D1.ID_DRUZYNY
      LEFT JOIN DRUZYNY D2 ON S.ID_DRUZYNY_2 = D2.ID_DRUZYNY
    WHERE
      I.ID_IMPREZY = P_ID_IMPREZY
    ORDER BY
      S.DATA ASC
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END WYNIKI_LISTA;

FUNCTION DAJ_WYNIK_SPOTKANIA(P_ID_SPOTKANIA IN NUMBER) RETURN T_WYNIK_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT
      S.ID_SPOTKANIA,
      F.NAZWA F_NAZWA,
      S.NUMER S_NUMER,
      S.DATA S_DATA,
      S.WYNIK_1 S_WYNIK_1,
      S.WYNIK_2 S_WYNIK_2,
      D1.NAZWA D1_NAZWA,
      D2.NAZWA D2_NAZWA,
      D1.FLAGA D1_FLAGA,
      D2.FLAGA D2_FLAGA
    FROM
      SPOTKANIA S
      JOIN FAZY F ON S.ID_FAZY = F.ID_FAZY
      LEFT JOIN DRUZYNY D1 ON S.ID_DRUZYNY_1 = D1.ID_DRUZYNY
      LEFT JOIN DRUZYNY D2 ON S.ID_DRUZYNY_2 = D2.ID_DRUZYNY
    WHERE
      S.ID_SPOTKANIA = P_ID_SPOTKANIA
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END DAJ_WYNIK_SPOTKANIA;

FUNCTION TYPY_LISTA(P_ID_IMPREZY IN NUMBER, P_ID_UZYTKOWNICY IN NUMBER) RETURN T_TYP_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT
      S.ID_SPOTKANIA,
      T.ID_TYPY,
      F.NAZWA F_NAZWA,
      S.NUMER S_NUMER,
      S.DATA S_DATA,
      S.WYNIK_1 S_WYNIK_1,
      S.WYNIK_2 S_WYNIK_2,
      D1.NAZWA D1_NAZWA,
      D2.NAZWA D2_NAZWA,
      D1.FLAGA D1_FLAGA,
      D2.FLAGA D2_FLAGA,
      U.LOGIN U_LOGIN,
      U.NAZWA U_NAZWA,
      T.DATA T_DATA,
      T.WYNIK_1 T_WYNIK_1,
      T.WYNIK_2 T_WYNIK_2,
      T.PUNKTY T_PUNKTY,
      (CASE WHEN SYSDATE > S.DATA - C_MAKS_GLOSOWANIE THEN 0 ELSE 1 END) MOZNA_TYPOWAC
    FROM
      SPOTKANIA S
      JOIN FAZY F ON S.ID_FAZY = F.ID_FAZY
      JOIN IMPREZY I ON F.ID_IMPREZY = I.ID_IMPREZY
      LEFT JOIN DRUZYNY D1 ON S.ID_DRUZYNY_1 = D1.ID_DRUZYNY
      LEFT JOIN DRUZYNY D2 ON S.ID_DRUZYNY_2 = D2.ID_DRUZYNY
      LEFT JOIN TYPY T ON S.ID_SPOTKANIA = T.ID_SPOTKANIA AND T.ID_UZYTKOWNICY = P_ID_UZYTKOWNICY
      LEFT JOIN UZYTKOWNICY U ON T.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY
    WHERE
      I.ID_IMPREZY = P_ID_IMPREZY
    ORDER BY
      /*S.DATA*/S.NUMER ASC
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END TYPY_LISTA;

FUNCTION WYNIKI_SPOTKANIA_LISTA(P_ID_SPOTKANIA IN NUMBER) RETURN T_WYNIK_SPOTKANIA_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT
      S.ID_SPOTKANIA,
      T.ID_TYPY,
      S.NUMER S_NUMER,
      S.DATA S_DATA,
      D1.NAZWA D1_NAZWA,
      D2.NAZWA D2_NAZWA,
      D1.FLAGA D1_FLAGA,
      D2.FLAGA D2_FLAGA,
      S.WYNIK_1 S_WYNIK_1,
      S.WYNIK_2 S_WYNIK_2,
      T.DATA T_DATA,
      T.WYNIK_1 T_WYNIK_1,
      T.WYNIK_2 T_WYNIK_2,
      NVL(T.PUNKTY, 0) PUNKTY,
      U.LOGIN U_LOGIN,
      U.NAZWA U_NAZWA
    FROM
      UZYTKOWNICY U
      JOIN SPOTKANIA S ON S.ID_SPOTKANIA = P_ID_SPOTKANIA
      LEFT JOIN TYPY T ON T.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY
      LEFT JOIN DRUZYNY D1 ON D1.ID_DRUZYNY = S.ID_DRUZYNY_1
      LEFT JOIN DRUZYNY D2 ON D2.ID_DRUZYNY = S.ID_DRUZYNY_2
    WHERE
      (T.ID_SPOTKANIA IS NULL OR T.ID_SPOTKANIA = P_ID_SPOTKANIA)
      AND NOT EXISTS (SELECT GU.LOGIN FROM GRUPY_UZYTKOWNIKOW GU WHERE GU.LOGIN = U.LOGIN AND GU.GRUPA = C_ROLA_ADMINUSER)
    ORDER BY
      T.PUNKTY DESC NULLS LAST, U.NAZWA
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END WYNIKI_SPOTKANIA_LISTA;

FUNCTION RANKING_LISTA(P_ID_IMPREZY IN NUMBER, P_NA_DZIEN IN DATE DEFAULT SYSDATE) RETURN T_RANKING_T PIPELINED IS
  L_ID_DRUZYNY_MISTRZ NUMBER;
  L_PUNKTY_STRATA_DO_POPRZ NUMBER;
BEGIN
  SELECT ID_DRUZYNY_MISTRZ INTO L_ID_DRUZYNY_MISTRZ FROM IMPREZY WHERE ID_IMPREZY = P_ID_IMPREZY;

  FOR REC IN
  (
    SELECT
      T.ID_UZYTKOWNICY,
      T.LOGIN,
      T.NAZWA,
      (T.PUNKTY_SUMA + T.PUNKTY_MISTRZ) AS PUNKTY_SUMA,
      GREATEST(LAG(T.PUNKTY_SUMA + T.PUNKTY_MISTRZ, 1, 0) OVER (ORDER BY (T.PUNKTY_SUMA + T.PUNKTY_MISTRZ) DESC) - (T.PUNKTY_SUMA + T.PUNKTY_MISTRZ), 0) AS PUNKTY_STRATA_DO_POPRZ,
      FIRST_VALUE(T.PUNKTY_SUMA + T.PUNKTY_MISTRZ) OVER (ORDER BY (T.PUNKTY_SUMA + T.PUNKTY_MISTRZ) DESC) - (T.PUNKTY_SUMA + T.PUNKTY_MISTRZ) AS PUNKTY_STRATA_DO_PIERW,
      T.PUNKTY_MISTRZ,
      T.PUNKTY_ILOSC_3,
      T.PUNKTY_ILOSC_1,
      RANK() OVER (ORDER BY (T.PUNKTY_SUMA + T.PUNKTY_MISTRZ) DESC, PUNKTY_MISTRZ DESC, PUNKTY_ILOSC_3 DESC, PUNKTY_ILOSC_1 DESC) POZYCJA
    FROM
    (
      SELECT
        U.ID_UZYTKOWNICY,
        U.LOGIN,
        U.NAZWA,
        (SELECT -- PUNKTY_SUMA
          NVL(SUM(PUNKTY), 0) AS PUNKTY_SUMA
        FROM
          TYPY T
          JOIN SPOTKANIA S ON S.ID_SPOTKANIA = T.ID_SPOTKANIA
          JOIN FAZY F ON F.ID_FAZY = S.ID_FAZY
        WHERE
          F.ID_IMPREZY = P_ID_IMPREZY
          AND TRUNC(S.DATA) <= TRUNC(P_NA_DZIEN)
          AND T.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY) PUNKTY_SUMA,
        (CASE -- PUNKTY_MISTRZ
          WHEN (SELECT COUNT(*)
                FROM IMPREZY_UZYTKOWNICY IU
                JOIN IMPREZY I ON IU.ID_IMPREZY = I.ID_IMPREZY
                WHERE
                  IU.ID_IMPREZY = P_ID_IMPREZY
                  AND TRUNC(I.DATA_KONCA) <= TRUNC(P_NA_DZIEN)
                  AND IU.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY
                  AND IU.ID_DRUZYNY_MISTRZ = L_ID_DRUZYNY_MISTRZ) > 0
            THEN C_PUNKTY_MISTRZ
          ELSE 0 END) PUNKTY_MISTRZ,
        (SELECT -- PUNKTY_ILOSC_3
          COUNT(*)
        FROM
          TYPY T
          JOIN SPOTKANIA S ON S.ID_SPOTKANIA = T.ID_SPOTKANIA
          JOIN FAZY F ON F.ID_FAZY = S.ID_FAZY
        WHERE
          F.ID_IMPREZY = P_ID_IMPREZY
          AND TRUNC(S.DATA) <= TRUNC(P_NA_DZIEN)
          AND T.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY
          AND T.PUNKTY = C_PUNKTY_3) PUNKTY_ILOSC_3,
        (SELECT -- PUNKTY_ILOSC_1
          COUNT(*)
        FROM
          TYPY T
          JOIN SPOTKANIA S ON S.ID_SPOTKANIA = T.ID_SPOTKANIA
          JOIN FAZY F ON F.ID_FAZY = S.ID_FAZY
        WHERE
          F.ID_IMPREZY = P_ID_IMPREZY
          AND TRUNC(S.DATA) <= TRUNC(P_NA_DZIEN)
          AND T.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY
          AND T.PUNKTY = C_PUNKTY_1) PUNKTY_ILOSC_1
      FROM
        UZYTKOWNICY U
        JOIN IMPREZY_UZYTKOWNICY IU ON U.ID_UZYTKOWNICY = IU.ID_UZYTKOWNICY
      WHERE
        U.AKTYWNY = 'T'
        AND IU.ID_IMPREZY = P_ID_IMPREZY
        AND NOT EXISTS (SELECT GU.LOGIN FROM GRUPY_UZYTKOWNIKOW GU WHERE GU.LOGIN = U.LOGIN AND GU.GRUPA = C_ROLA_ADMINUSER)
    ) T
    ORDER BY
      POZYCJA,
      NAZWA
  )
  LOOP
    IF L_PUNKTY_STRATA_DO_POPRZ IS NULL THEN
      L_PUNKTY_STRATA_DO_POPRZ := REC.PUNKTY_STRATA_DO_POPRZ;
    ELSE
      IF REC.PUNKTY_STRATA_DO_POPRZ = 0 THEN
        REC.PUNKTY_STRATA_DO_POPRZ := L_PUNKTY_STRATA_DO_POPRZ;
      ELSE
        L_PUNKTY_STRATA_DO_POPRZ := REC.PUNKTY_STRATA_DO_POPRZ;
      END IF;
    END IF;

    PIPE ROW(REC);
  END LOOP;
END RANKING_LISTA;

FUNCTION RANKING_PELNY_LISTA(P_ID_IMPREZY IN NUMBER, P_NA_DZIEN IN DATE DEFAULT SYSDATE) RETURN T_RANKING_PELNY_T PIPELINED IS
  L_OSTATNIA_DATA DATE;
  L_PRZEDOSTATNIA_DATA DATE;
  TYPE MAPA_IDX_POPRZ_RANK_T IS TABLE OF PLS_INTEGER INDEX BY PLS_INTEGER;
  L_IDX_POPRZ_RANK MAPA_IDX_POPRZ_RANK_T;
  L_POPRZ_RANKINGI T_RANKING_T;
BEGIN
  -- wyci�gni�cie ostatniej daty spotka�
  SELECT MAX(TRUNC(DATA)) INTO L_OSTATNIA_DATA FROM SPOTKANIA WHERE WYNIK_1 IS NOT NULL AND WYNIK_2 IS NOT NULL AND TRUNC(DATA) <= TRUNC(COALESCE(P_NA_DZIEN, SYSDATE));

  -- wyci�gni�cie przedostatniej daty spotka�
  SELECT MAX(TRUNC(DATA)) INTO L_PRZEDOSTATNIA_DATA FROM SPOTKANIA WHERE WYNIK_1 IS NOT NULL AND WYNIK_2 IS NOT NULL AND TRUNC(DATA) < TRUNC(COALESCE(L_OSTATNIA_DATA, SYSDATE));

  -- ranking na przedostatni� dat� spotka�
  L_POPRZ_RANKINGI := T_RANKING_T();
  IF L_PRZEDOSTATNIA_DATA IS NOT NULL THEN
    FOR REC IN
    (
      SELECT * FROM TABLE(RANKING_LISTA(P_ID_IMPREZY, L_PRZEDOSTATNIA_DATA))
    )
    LOOP
      L_POPRZ_RANKINGI.EXTEND;
      L_POPRZ_RANKINGI(L_POPRZ_RANKINGI.COUNT) := REC;
      L_IDX_POPRZ_RANK(REC.ID_UZYTKOWNICY) := L_POPRZ_RANKINGI.COUNT;
    END LOOP;
  END IF;

  -- ranking na ostatni� dat� spotka�
  FOR REC IN
  (
    SELECT
      T.*,
      0 AS POPRZEDNIA_POZYCJA,
      0 AS ZMIANA_POZYCJI,
      0 AS POPRZEDNIE_PUNKTY_SUMA,
      0 AS ZMIANA_PUNKTOW
    FROM TABLE(RANKING_LISTA(P_ID_IMPREZY, L_OSTATNIA_DATA)) T
  )
  LOOP
    IF L_IDX_POPRZ_RANK.EXISTS(REC.ID_UZYTKOWNICY) THEN
      REC.POPRZEDNIA_POZYCJA := L_POPRZ_RANKINGI(L_IDX_POPRZ_RANK(REC.ID_UZYTKOWNICY)).POZYCJA;
      REC.ZMIANA_POZYCJI := REC.POPRZEDNIA_POZYCJA - REC.POZYCJA;
      REC.POPRZEDNIE_PUNKTY_SUMA := L_POPRZ_RANKINGI(L_IDX_POPRZ_RANK(REC.ID_UZYTKOWNICY)).PUNKTY_SUMA;
      REC.ZMIANA_PUNKTOW := REC.PUNKTY_SUMA - REC.POPRZEDNIE_PUNKTY_SUMA;
    ELSE
      REC.POPRZEDNIA_POZYCJA := REC.POZYCJA;
      REC.POPRZEDNIE_PUNKTY_SUMA := REC.PUNKTY_SUMA;
    END IF;

    PIPE ROW(REC);
  END LOOP;
END RANKING_PELNY_LISTA;

FUNCTION UZYTKOWNICY_LISTA(P_ID_IMPREZY IN NUMBER) RETURN T_UZYTKOWNIK_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT
      U.*
    FROM
      IMPREZY_UZYTKOWNICY IU
      JOIN UZYTKOWNICY U ON IU.ID_UZYTKOWNICY = U.ID_UZYTKOWNICY
    WHERE
      IU.ID_IMPREZY = P_ID_IMPREZY
      AND U.AKTYWNY = 'T'
      AND NOT EXISTS (SELECT GU.LOGIN FROM GRUPY_UZYTKOWNIKOW GU WHERE GU.LOGIN = U.LOGIN AND GU.GRUPA = C_ROLA_ADMINUSER)
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END UZYTKOWNICY_LISTA;

FUNCTION DAJ_DANE_UZYTKOWNIKA(P_ID_UZYTKOWNICY IN NUMBER) RETURN T_UZYTKOWNIK_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT U.* FROM UZYTKOWNICY U WHERE U.ID_UZYTKOWNICY = P_ID_UZYTKOWNICY
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END DAJ_DANE_UZYTKOWNIKA;


FUNCTION RANKING_HIST_LISTA(P_ID_IMPREZY IN NUMBER, P_ID_UZYTKOWNICY IN NUMBER) RETURN T_RANKING_HIST_T PIPELINED IS
BEGIN
  FOR REC_DZIEN IN
  (
    SELECT DISTINCT(Trunc(DATA)) AS DZIEN FROM SPOTKANIA WHERE Trunc(DATA) <= Trunc(SYSDATE) ORDER BY Trunc(DATA)
  )
  LOOP
    FOR REC IN
    (
      SELECT
        To_Char(REC_DZIEN.DZIEN, 'dd.mm') AS OS_X_OPIS,
        POZYCJA,
        PUNKTY_SUMA
      FROM TABLE(RANKING_LISTA(P_ID_IMPREZY, REC_DZIEN.DZIEN)) WHERE ID_UZYTKOWNICY = P_ID_UZYTKOWNICY
    )
    LOOP
      PIPE ROW(REC);
    END LOOP;
  END LOOP;
END RANKING_HIST_LISTA;

FUNCTION TYPY_MISTRZ_LISTA(P_ID_IMPREZY IN NUMBER) RETURN T_TYP_MISTRZ_T PIPELINED IS
BEGIN
  FOR REC IN
  (
    SELECT
      IU.ID_UZYTKOWNICY AS ID_UZYTKOWNICY,
      IU.ID_DRUZYNY_MISTRZ AS ID_DRUZYNY,
      IU.DATA_TYPOWANIA_MISTRZ AS DATA_TYPOWANIA,
      U.LOGIN AS U_LOGIN,
      U.NAZWA AS U_NAZWA,
      D.NAZWA AS D_NAZWA,
      D.FLAGA AS D_FLAGA
    FROM
      IMPREZY_UZYTKOWNICY IU
      JOIN UZYTKOWNICY U ON U.ID_UZYTKOWNICY = IU.ID_UZYTKOWNICY
      LEFT JOIN DRUZYNY D ON D.ID_DRUZYNY = IU.ID_DRUZYNY_MISTRZ
    WHERE
      IU.ID_IMPREZY = P_ID_IMPREZY
      AND NOT EXISTS (SELECT GU.LOGIN FROM GRUPY_UZYTKOWNIKOW GU WHERE GU.LOGIN = U.LOGIN AND GU.GRUPA = C_ROLA_ADMINUSER)
    ORDER BY
      U.NAZWA
  )
  LOOP
    PIPE ROW(REC);
  END LOOP;
END TYPY_MISTRZ_LISTA;

PROCEDURE TYPUJ_WYNIK(P_ID_UZYTKOWNICY IN NUMBER, P_ID_SPOTKANIA IN NUMBER,  P_TYP_D1 IN NUMBER, P_TYP_D2 IN NUMBER, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F') IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_MOZNA_TYPOWAC NUMBER;
  L_ID_TYPY NUMBER;
BEGIN
  -- sprawdzenie czy nie min�� czas typowania
  IF P_POZWOL_PO_CZASIE <> 'T' THEN
    SELECT (CASE WHEN SYSDATE > DATA - C_MAKS_GLOSOWANIE THEN 0 ELSE 1 END) INTO L_MOZNA_TYPOWAC FROM SPOTKANIA WHERE ID_SPOTKANIA = P_ID_SPOTKANIA;
    IF L_MOZNA_TYPOWAC <> 1 THEN
      LOGUJ(P_ID_UZYTKOWNICY, 'TYPUJ_WYNIK', 'Up�yn�� czas na oddanie g�osu (ID_SPOTKANIA:'||P_ID_SPOTKANIA||')');
      Raise_Application_Error(-20000, 'Min�� czas na oddanie typu na podane spotkanie!');
    END IF;
  END IF;

  BEGIN
    SELECT ID_TYPY INTO L_ID_TYPY FROM TYPY WHERE ID_SPOTKANIA = P_ID_SPOTKANIA AND ID_UZYTKOWNICY = P_ID_UZYTKOWNICY;
    UPDATE TYPY SET DATA = SYSDATE, WYNIK_1 = P_TYP_D1, WYNIK_2 = P_TYP_D2 WHERE ID_TYPY = L_ID_TYPY;
    LOGUJ(P_ID_UZYTKOWNICY, 'TYPUJ_WYNIK', 'UPDATE, ID_SPOTKANIA:'||P_ID_SPOTKANIA||', ID_TYPY:'||L_ID_TYPY||', TYP_D1:'||P_TYP_D1||', TYP_D2:'||P_TYP_D2);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      INSERT INTO TYPY (ID_SPOTKANIA, ID_UZYTKOWNICY, DATA, WYNIK_1, WYNIK_2) VALUES (P_ID_SPOTKANIA, P_ID_UZYTKOWNICY, SYSDATE, P_TYP_D1, P_TYP_D2);
      LOGUJ(P_ID_UZYTKOWNICY, 'TYPUJ_WYNIK', 'INSERT, ID_SPOTKANIA:'||P_ID_SPOTKANIA||', TYP_D1:'||P_TYP_D1||', TYP_D2:'||P_TYP_D2);
  END;
  COMMIT;
END TYPUJ_WYNIK;

PROCEDURE TYPUJ_WYNIK2(P_ID_IMPREZY IN NUMBER, P_LOGIN IN VARCHAR2, P_NUMER_SPOTKANIA IN NUMBER, P_TYP_D1 IN NUMBER, P_TYP_D2 IN NUMBER, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F') IS
  L_ID_SPOTKANIA NUMBER;
  L_ID_UZYTKOWNICY NUMBER;
BEGIN
  SELECT S.ID_SPOTKANIA INTO L_ID_SPOTKANIA FROM SPOTKANIA S JOIN FAZY F ON S.ID_FAZY = F.ID_FAZY WHERE F.ID_IMPREZY = P_ID_IMPREZY AND S.NUMER = P_NUMER_SPOTKANIA;
  SELECT ID_UZYTKOWNICY INTO L_ID_UZYTKOWNICY FROM UZYTKOWNICY WHERE LOGIN = P_LOGIN;

  TYPUJ_WYNIK(P_ID_UZYTKOWNICY => L_ID_UZYTKOWNICY, P_ID_SPOTKANIA => L_ID_SPOTKANIA, P_TYP_D1 => P_TYP_D1, P_TYP_D2 => P_TYP_D2, P_POZWOL_PO_CZASIE => P_POZWOL_PO_CZASIE);
END TYPUJ_WYNIK2;

PROCEDURE TYPUJ_MISTRZA(P_ID_IMPREZY IN NUMBER, P_LOGIN IN VARCHAR2, P_DRUZYNA_MISTRZ IN VARCHAR2, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F') IS
  V_ID_UZYTKOWNICY NUMBER;
  V_ID_DRUZYNY NUMBER;
BEGIN
  SELECT ID_UZYTKOWNICY INTO V_ID_UZYTKOWNICY FROM UZYTKOWNICY WHERE LOGIN = P_LOGIN;
  IF P_DRUZYNA_MISTRZ IS NOT NULL THEN
    SELECT ID_DRUZYNY INTO V_ID_DRUZYNY FROM DRUZYNY WHERE NAZWA = P_DRUZYNA_MISTRZ;
  END IF;
  TYPUJ_MISTRZA2(P_ID_IMPREZY, V_ID_UZYTKOWNICY, V_ID_DRUZYNY, P_POZWOL_PO_CZASIE);
END TYPUJ_MISTRZA;

PROCEDURE TYPUJ_MISTRZA2(P_ID_IMPREZY IN NUMBER, P_ID_UZYTKOWNICY IN NUMBER, P_ID_DRUZYNY_MISTRZ IN NUMBER, P_POZWOL_PO_CZASIE IN CHAR DEFAULT 'F') IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_MOZNA_TYPOWAC NUMBER;
BEGIN
  -- sprawdzenie czy nie min�� czas typowania
  IF P_POZWOL_PO_CZASIE <> 'T' THEN
    SELECT (CASE WHEN SYSDATE >= COALESCE(MAKS_DATA_TYPOWANIA_MISTRZA, SYSDATE) /*- C_MAKS_GLOSOWANIE*/ THEN 0 ELSE 1 END) INTO L_MOZNA_TYPOWAC FROM IMPREZY WHERE ID_IMPREZY = P_ID_IMPREZY;
    IF L_MOZNA_TYPOWAC <> 1 THEN
      LOGUJ(P_ID_UZYTKOWNICY, 'TYPUJ_MISTRZA', 'Up�yn�� czas na wytypowanie mistrza');
      Raise_Application_Error(-20000, 'Min�� czas na wytypowanie mistrza!');
    END IF;
  END IF;

  UPDATE
    IMPREZY_UZYTKOWNICY
  SET
    ID_DRUZYNY_MISTRZ = P_ID_DRUZYNY_MISTRZ,
    DATA_TYPOWANIA_MISTRZ = SYSDATE
  WHERE
    ID_IMPREZY = P_ID_IMPREZY
    AND ID_UZYTKOWNICY = P_ID_UZYTKOWNICY;

  LOGUJ(P_ID_UZYTKOWNICY, 'TYPUJ_MISTRZA', 'ID_DRUZYNY_MISTRZ:'||P_ID_DRUZYNY_MISTRZ);
  COMMIT;
END TYPUJ_MISTRZA2;

PROCEDURE UAKTUALNIJ_WYNIK(P_ID_SPOTKANIA IN NUMBER, P_WYNIK_D1 IN NUMBER, P_WYNIK_D2 IN NUMBER) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  UPDATE SPOTKANIA SET WYNIK_1 = P_WYNIK_D1, WYNIK_2 = P_WYNIK_D2 WHERE ID_SPOTKANIA = P_ID_SPOTKANIA;
  LOGUJ(NULL, 'UAKTUALNIJ_WYNIK', 'ID_SPOTKANIA:'||P_ID_SPOTKANIA||', WYNIK_D1:'||P_WYNIK_D1||', WYNIK_D2:'||P_WYNIK_D2);
  COMMIT;
  PRZELICZ_PUNKTY;
END UAKTUALNIJ_WYNIK;

PROCEDURE PRZELICZ_PUNKTY IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_PUNKTY NUMBER;
BEGIN
  FOR REC IN
  (
    SELECT
      T.ID_TYPY,
      S.WYNIK_1 S_WYNIK_1,
      S.WYNIK_2 S_WYNIK_2,
      T.WYNIK_1 T_WYNIK_1,
      T.WYNIK_2 T_WYNIK_2
    FROM
      TYPY T
      JOIN SPOTKANIA S ON T.ID_SPOTKANIA = S.ID_SPOTKANIA
  )
  LOOP
    L_PUNKTY := OBLICZ_PUNKTY(REC.S_WYNIK_1, REC.S_WYNIK_2, REC.T_WYNIK_1, REC.T_WYNIK_2);
    UPDATE TYPY SET PUNKTY = L_PUNKTY WHERE ID_TYPY = REC.ID_TYPY;
  END LOOP;
  COMMIT;
END PRZELICZ_PUNKTY;

PROCEDURE DODAJ_SPOTKANIE(P_ID_IMPREZY IN NUMBER, P_FAZA IN VARCHAR2, P_NUMER IN NUMBER, P_DATA_SPOTKANIA IN VARCHAR, P_DRUZYNA_1 IN VARCHAR2, P_DRUZYNA_2 IN VARCHAR2, P_OPIS IN VARCHAR2 DEFAULT NULL) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_TMP NUMBER;
  L_ID_FAZY NUMBER;
  L_NUMER NUMBER;
  L_DATA_SPOTKANIA DATE;
  L_ID_DRUZYNY_1 NUMBER;
  L_ID_DRUZYNY_2 NUMBER;
BEGIN
  SELECT ID_FAZY INTO L_ID_FAZY FROM FAZY WHERE ID_IMPREZY = P_ID_IMPREZY AND UPPER(NAZWA) = UPPER(P_FAZA);

  L_NUMER := P_NUMER;
  IF L_NUMER IS NULL THEN
    SELECT Max(S.NUMER)+1 INTO L_NUMER FROM SPOTKANIA S JOIN FAZY F ON F.ID_FAZY = S.ID_FAZY WHERE F.ID_IMPREZY = P_ID_IMPREZY;
  ELSE
    SELECT Count(*) INTO L_TMP FROM SPOTKANIA S JOIN FAZY F ON F.ID_FAZY = S.ID_FAZY WHERE F.ID_IMPREZY = P_ID_IMPREZY AND S.NUMER = L_NUMER;
    IF L_TMP > 0 THEN
      Raise_Application_Error(-20000, 'Ju� jest spotkanie z podanym numerem!');
    END IF;
  END IF;

  L_DATA_SPOTKANIA := TO_DATE(P_DATA_SPOTKANIA, 'YYYY-MM-DD HH24:MI');

  IF P_DRUZYNA_1 IS NOT NULL THEN
    SELECT ID_DRUZYNY INTO L_ID_DRUZYNY_1 FROM DRUZYNY WHERE UPPER(NAZWA) = UPPER(P_DRUZYNA_1);
  END IF;

  IF P_DRUZYNA_2 IS NOT NULL THEN
    SELECT ID_DRUZYNY INTO L_ID_DRUZYNY_2 FROM DRUZYNY WHERE UPPER(NAZWA) = UPPER(P_DRUZYNA_2);
  END IF;

  INSERT INTO SPOTKANIA (ID_FAZY, NUMER, DATA, ID_DRUZYNY_1, ID_DRUZYNY_2, OPIS) VALUES (L_ID_FAZY, L_NUMER, L_DATA_SPOTKANIA, L_ID_DRUZYNY_1, L_ID_DRUZYNY_2, P_OPIS);
  COMMIT;
END DODAJ_SPOTKANIE;

PROCEDURE UAKTUALNIJ_SPOTKANIE(P_ID_IMPREZY IN NUMBER, P_NUMER IN NUMBER, P_DRUZYNA_1 IN VARCHAR2, P_DRUZYNA_2 IN VARCHAR2) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_ID_SPOTKANIA NUMBER;
  L_ID_DRUZYNY_1 NUMBER;
  L_ID_DRUZYNY_2 NUMBER;
BEGIN
  SELECT S.ID_SPOTKANIA INTO L_ID_SPOTKANIA
  FROM
    SPOTKANIA S
    JOIN FAZY F ON S.ID_FAZY = F.ID_FAZY
  WHERE
    F.ID_IMPREZY = P_ID_IMPREZY
    AND S.NUMER = P_NUMER;

  IF P_DRUZYNA_1 IS NOT NULL THEN
    SELECT ID_DRUZYNY INTO L_ID_DRUZYNY_1 FROM DRUZYNY WHERE UPPER(NAZWA) = UPPER(P_DRUZYNA_1);
  END IF;

  IF P_DRUZYNA_2 IS NOT NULL THEN
    SELECT ID_DRUZYNY INTO L_ID_DRUZYNY_2 FROM DRUZYNY WHERE UPPER(NAZWA) = UPPER(P_DRUZYNA_2);
  END IF;

  UPDATE SPOTKANIA SET ID_DRUZYNY_1 = L_ID_DRUZYNY_1, ID_DRUZYNY_2 = L_ID_DRUZYNY_2 WHERE ID_SPOTKANIA = L_ID_SPOTKANIA;
  COMMIT;
END UAKTUALNIJ_SPOTKANIE;

FUNCTION MD5(PASSWD IN VARCHAR2) RETURN VARCHAR2 IS
  RET VARCHAR2(32);
BEGIN
  RET := LOWER(RAWTOHEX(UTL_RAW.CAST_TO_RAW(DBMS_OBFUSCATION_TOOLKIT.MD5(INPUT_STRING => PASSWD))));
  RETURN RET;
END;

PROCEDURE DODAJ_UZYTKOWNIKA(P_LOGIN IN VARCHAR2, P_HASLO IN VARCHAR2, P_HASH IN CHAR, P_ID_IMPREZY IN NUMBER DEFAULT NULL, P_NAZWA IN VARCHAR2 DEFAULT NULL, P_ADMIN IN CHAR DEFAULT 'F') IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_ID_UZYTKOWNICY UZYTKOWNICY.ID_UZYTKOWNICY%TYPE;
  L_HASLO UZYTKOWNICY.HASLO%TYPE;
BEGIN
  SELECT G_UZYTKOWNICY.NEXTVAL INTO L_ID_UZYTKOWNICY FROM DUAL;
  IF P_HASH <> 'T' THEN
    L_HASLO := MD5(P_HASLO);
  ELSE
    L_HASLO := P_HASLO;
  END IF;
  INSERT INTO UZYTKOWNICY (ID_UZYTKOWNICY, LOGIN, HASLO, NAZWA) VALUES (L_ID_UZYTKOWNICY, P_LOGIN, L_HASLO, P_NAZWA);
  INSERT INTO GRUPY_UZYTKOWNIKOW (LOGIN, GRUPA) VALUES (P_LOGIN, C_ROLA_NORMALUSER);
  IF P_ADMIN = 'T' THEN
    INSERT INTO GRUPY_UZYTKOWNIKOW (LOGIN, GRUPA) VALUES (P_LOGIN, C_ROLA_ADMINUSER);
  END IF;
  IF P_ID_IMPREZY IS NOT NULL THEN
    INSERT INTO IMPREZY_UZYTKOWNICY (ID_IMPREZY, ID_UZYTKOWNICY) VALUES (P_ID_IMPREZY, L_ID_UZYTKOWNICY);
  END IF;
  LOGUJ(L_ID_UZYTKOWNICY, 'DODAJ_UZYTKOWNIKA', 'LOGIN: '||P_LOGIN||', ADMIN: '||P_ADMIN);
  COMMIT;
END DODAJ_UZYTKOWNIKA;

PROCEDURE USTAW_HASLO_UZYTKOWNIKA(P_LOGIN IN VARCHAR2, P_HASLO IN VARCHAR2) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_ID_UZYTKOWNICY UZYTKOWNICY.ID_UZYTKOWNICY%TYPE;
  L_HASLO UZYTKOWNICY.HASLO%TYPE;
BEGIN
  SELECT ID_UZYTKOWNICY INTO L_ID_UZYTKOWNICY FROM UZYTKOWNICY WHERE Upper(LOGIN) = Upper(P_LOGIN);

  L_HASLO := MD5(P_HASLO);
  UPDATE UZYTKOWNICY SET HASLO = L_HASLO WHERE ID_UZYTKOWNICY = L_ID_UZYTKOWNICY;
  LOGUJ(L_ID_UZYTKOWNICY, 'USTAW_HASLO_UZYTKOWNICY', 'LOGIN: '||P_LOGIN);
  COMMIT;
END USTAW_HASLO_UZYTKOWNIKA;

END CASINO;
/

